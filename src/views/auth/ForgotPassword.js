import React, { useState } from "react";
import { SERVER_URL } from "../../constants";
import { ToastContainer, toast } from "react-toastify";

import Home from "layouts/Home";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";

const ResetPassword = () => {
  const history = useHistory();
  const [username, setUsername] = useState({ username: "" });
  const [token, setToken] = useState({ token: "" });
  const [password, setPassword] = useState({ password: "" });
  const [reset, setReset] = useState(false);

  const handleChange = (event) => {
    setUsername({ username, [event.target.name]: event.target.value });
  };
  const handlePasswordChange = (event) => {
    setPassword({ password, [event.target.name]: event.target.value });
  };

  const ForgotPassword = () => {
    fetch(SERVER_URL + "forgot-password?username=" + username.username, {
      method: "POST",
    })
      .then((res) => {
        console.log(`Token :`, res.headers.get("x-token"));
        const newtoken = res.headers.get("x-token");
        if (newtoken != "" || newtoken != null) {
          setToken({ token: res.headers.get("x-token") });
          setReset(true);
        } else {
          toast.error("Error while genarating token", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        console.log(`err forgot password`, err);
      });
  };

  const ResetPassword = () => {
    fetch(
      SERVER_URL +
        "reset-password?password=" +
        password.password +
        "&token=" +
        token.token,
      {
        method: "POST",
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Password changed succesfully!", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          setReset(false);
          history.push({
            pathname: "/auth/login",
          });
        }
      })
      .catch((err) => {
        toast.error("Error while resetting a password", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };

  return (
    <>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-5/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="rounded-t mb-0 px-6 py-6">
                <div className="text-center mb-3">
                  <h1 className="Gray-900 font-bold">Reset Password</h1>
                </div>

                <hr className="my-2 border-b-1 border-blueGray-300" />
              </div>
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <form>
                  <div className="relative w-full mb-3">
                    <label className="block uppercase text-blueGray-600 text-xs font-bold mb-2">
                      Username
                    </label>
                    <input
                      type="email"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Username"
                      name="username"
                      onChange={handleChange}
                    />
                  </div>
                  <div style={{ display: reset == true ? "block" : "none" }}>
                    <div className="relative w-full mb-3">
                      <label className="block uppercase text-blueGray-600 text-xs font-bold mb-2">
                        Token
                      </label>
                      <input
                        type="text"
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholder="Token"
                        name="token"
                        value={token.token}
                      />
                    </div>

                    <div className="relative w-full mb-3">
                      <label className="block uppercase text-blueGray-600 text-xs font-bold mb-2">
                        New Password
                      </label>
                      <input
                        id="newpassword"
                        type="password"
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholder="Password"
                        name="password"
                        onChange={handlePasswordChange}
                        value={password.password}
                        //onChange={(e) => setPassword(e.target.value)}
                      />
                    </div>
                    <div className="text-center mt-6">
                      <button
                        className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                        type="button"
                        onClick={ResetPassword}
                      >
                        Reset Password
                      </button>
                    </div>
                  </div>{" "}
                  <div style={{ display: reset == false ? "block" : "none" }}>
                    <div className="text-center mt-6">
                      <button
                        className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                        type="button"
                        onClick={ForgotPassword}
                      >
                        Request Reset Token
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="flex flex-wrap mt-6 relative">
              <div className="w-1/2">
                <Link to="/auth/login" className="text-blueGray-200">
                  <small>Log in?</small>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={3000} />
    </>
  );
};
export default ResetPassword;
