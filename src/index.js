import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "assets/styles/tailwind.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/styles/flatpickr.scss";
// import "assets/styles/index.scss";
//import "bootstrap/dist/css/bootstrap.min.css";
//import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css"

import "./styles.css";
import "react-toastify/dist/ReactToastify.css";
// layouts

import Home from "layouts/Home.js";
import Auth from "layouts/Auth.js";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      {/* add routes with layouts */}
      <Route path="/dashboard" component={Home} />
      <Route path="/auth" component={Auth} />
      {/* add routes without layouts */}
      <Route path="/" exact component={Home} />
      {/* add redirect for first page */}
      <Redirect from="*" to="/dashboard" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
