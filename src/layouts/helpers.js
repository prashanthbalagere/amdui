export function loaderShow() {
  var loader = document.getElementById("loaderbtn");
  loader.style.display = "block";
  loader.style.opacity = "1";
  loader.style.visibility = "visible";
}

export function loaderHide() {
  setTimeout(function () {
    var loader = document.getElementById("loaderbtn");
    loader.style.transition = ".1s";
    loader.style.opacity = "0";
    loader.style.visibility = "hidden";
    loader.style.display = "none";
  }, 750);
}
export function loadermetricsShow() {
  var loader = document.getElementById("loadermetrics");
  loader.style.display = "block";
  loader.style.opacity = "1";
  loader.style.visibility = "visible";
}

export function loadermetricsHide() {
  setTimeout(function () {
    var loader = document.getElementById("loadermetrics");
    loader.style.transition = ".1s";
    loader.style.opacity = "0";
    loader.style.visibility = "hidden";
    loader.style.display = "none";
  }, 750);
}
