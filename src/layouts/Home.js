import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components

import AdminNavbar from "components/Navbars/AdminNavbar.js";
import HeaderStats from "components/Headers/HeaderStats.js";
import FooterAdmin from "components/Footers/FooterAdmin.js";
import SettingsTabs from "components/Metrics/Settings/SettingsTabs";
import GovernanceProgressTabs from "components/Metrics/Governance/GovernanceProgressTabs";
import TestDesignProgressTabs from "components/Metrics/TestDesign/TestDesignProgressTabs.js";
import TestExecutionProgressTabs from "components/Metrics/TestExecution/TestExecutionProgressTabs.js";
import MetricsTabs from "components/Metrics/Metricsheatmap/MetricsTabs";
import ALMSettings from "components/Metrics/ALMSettings/ALMSettings";
import JIRASettings from "components/Metrics/JIRASettings/JIRASettings";
import ELKDashboard from "components/ELK/ELKDashboard";
import ELKurls from "components/ELK/ELKurls";

// views

import Projectlist from "components/Metrics/Projectlist";
import MyModulesList from "components/Metrics/Modules/MyModulesList";
import ViewWeekly from "components/Metrics/ViewWeekly";
import Effort from "components/Metrics/Efforts/Efforts";

export default function Home() {
  return (
    <>
      <div
        id="loaderbtn"
        className="fixed m-auto top-10 w-52 bg-white py-5 px-10 rounded-xl shadow-lg"
        style={{ display: "none" }}
      >
        <div className="loader ease-linear rounded-full border-8 border-t-8 border-gray-200 w-12 h-12 text-center m-auto"></div>
        <div className="text-sm text-center mt-2">loading...</div>
      </div>
      <div className="relative">
        <AdminNavbar />
        {/* Header */}
        <div className="mx-auto w-full min-h-screen bg-metrics-bg">
          <div className="flex flex-wrap">
            <div className="w-full m-5  mt-16">
              <div className="relative flex flex-col min-w-0 break-words w-full mb-6 ">
                <div
                  id="loadermetrics"
                  className="fixed m-auto top-10 w-52 h-32 bg-white py-5 px-10 rounded-xl shadow-lg"
                  style={{ display: "none" }}
                >
                  <div className="text-center loader ease-linear rounded-full border-8 border-t-8 border-gray-200 w-12 h-12 m-auto"></div>
                  <div className="text-sm text-center mt-2 -m-8">
                    Generating Metrics...
                  </div>
                </div>
                <Switch>
                  <Route path="/dashboard" exact component={Projectlist} />
                  <Route
                    path="/dashboard/modules"
                    exact
                    component={MyModulesList}
                  />
                  <Route
                    path="/dashboard/estimate"
                    exact
                    component={SettingsTabs}
                  />
                  <Route
                    path="/dashboard/governance"
                    exact
                    component={GovernanceProgressTabs}
                  />
                  <Route
                    path="/dashboard/testdesign"
                    exact
                    component={TestDesignProgressTabs}
                  />
                  <Route
                    path="/dashboard/testexecution"
                    exact
                    component={TestExecutionProgressTabs}
                  />
                  <Route
                    path="/dashboard/metrics"
                    exact
                    component={MetricsTabs}
                  />
                  <Route
                    path="/dashboard/almsettings"
                    exact
                    component={ALMSettings}
                  />
                  <Route
                    path="/dashboard/jirasettings"
                    exact
                    component={JIRASettings}
                  />
                  <Route
                    path="/dashboard/summary"
                    exact
                    component={ViewWeekly}
                  />
                  <Route path="/dashboard/effort" exact component={Effort} />
                  <Route path="/dashboard/ELK" exact component={ELKDashboard} />
                  <Route path="/dashboard/ELKurls" exact component={ELKurls} />
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </div>
            </div>
          </div>

          <FooterAdmin />
        </div>
      </div>
    </>
  );
}
