import React from "react";
import { useHistory } from "react-router";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { dashboard_data } from "data";

export default function Navbar(props) {
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/auth/login",
    });
  };
  return (
    <>
      {/* Navbar */}
      <nav className="absolute top-0 left-0 w-full z-10 bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4 bg-gradient-to-br from-blue-900 to-pink-800 shadow-lg">
        <div className="w-full mx-autp items-center flex justify-between md:flex-nowrap flex-wrap px-1">
          <span className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <img
              src={require("assets/images/logo.png").default}
              alt={dashboard_data.name}
              className="img-responsive w-7"
            />
            <Link to="/" className="ml-3 text-xl text-white">
              {dashboard_data.name}
            </Link>
          </span>

          <ul className="flex-col md:flex-row list-none items-center hidden md:flex">
            {/* <UserDropdown /> */}
            <div className="text-right">
              <Menu as="div" className="relative inline-block text-left">
                <div>
                  <Menu.Button className="w-24 inline-flex justify-center  pr-4 py-2 text-sm font-medium text-white bg-black rounded-md bg-opacity-20 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75">
                    Admin
                    <i className="fas fa-angle-down absolute right-0 text-base ml-5 pt-0.5 pr-3"></i>
                  </Menu.Button>
                </div>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="absolute right-0 w-32 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="px-1 py-1 ">
                      <Menu.Item>
                        {({ active }) => (
                          <button
                            onClick={handleClose}
                            className={`${
                              active ? "bg-gray-200" : "text-gray-900"
                            } group flex rounded-md items-center w-full px-2 py-2 text-sm text-right`}
                          >
                            Logout
                          </button>
                        )}
                      </Menu.Item>
                    </div>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </ul>
        </div>
      </nav>
      {/* End Navbar */}
    </>
  );
}
