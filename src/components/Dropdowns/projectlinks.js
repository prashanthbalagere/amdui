import React from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
const Projectlinks = ({ rowvalue, onDashView, onDashURLs }) => {
  const onViewDashClick = () => {
    onDashView(rowvalue);
  };
  const onDashURLClick = () => {
    onDashURLs(rowvalue);
  };

  return (
    <>
      <ul className="flex-col md:flex-row list-none items-center hidden md:flex">
        {/* <UserDropdown /> */}
        <Menu as="div">
          <div>
            <Menu.Button className="optionsbtn bg-indigo-500 px-1 py-0.5 hover:bg-indigo-800 rounded">
              <span className="tooltiptext">Dashboard Settings</span>
              <i className="fas fa-chart-area text-white pt-0.5 u"></i>
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="absolute z-50 right-0 w-44 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div className="px-1 py-1 ">
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onViewDashClick}
                    >
                      <i className="fas fa-chart-area text-xs mr-1 w-4"></i>{" "}
                      Dashboard View
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800  dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onDashURLClick}
                    >
                      <i className="fas fa-link text-xs mr-1 w-4"></i> Dashboard
                      URL's
                    </button>
                  )}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      </ul>
    </>
  );
};

export default Projectlinks;
