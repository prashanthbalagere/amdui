import React from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";

const Projectoptions = ({
  rowvalue,
  project,
  onEditProject,
  addModule,
  onEstimate,
  onGovernance,
  onTestdesign,
  onTestexecution,
  onMetrics,
  onAlmsettings,
  onJirasettings,
  onSyncStartEnd,
  onSummary,
  onEffort,
}) => {
  const onEditProjectClick = () => {
    onEditProject(project, rowvalue);
  };
  const onSyncStartEndClick = () => {
    onSyncStartEnd(project, rowvalue);
  };
  const addModuleClick = () => {
    addModule(rowvalue);
  };
  const onEstimateClick = () => {
    onEstimate(rowvalue);
  };
  const onGovernanceClick = () => {
    onGovernance(rowvalue);
  };
  const onTestdesignClick = () => {
    onTestdesign(rowvalue);
  };
  const onTestexecutionClick = () => {
    onTestexecution(rowvalue);
  };
  const onMetricsClick = () => {
    onMetrics(rowvalue);
  };
  const onALMClick = () => {
    onAlmsettings(rowvalue);
  };
  const onJIRAClick = () => {
    onJirasettings(rowvalue);
  };
  const onSummaryClick = () => {
    onSummary(rowvalue);
  };
  const onEffortClick = () => {
    onEffort(rowvalue);
  };
  const [projectid, setprojectid] = React.useState(0);
  return (
    <>
      <ul className="flex-col md:flex-row list-none items-center hidden md:flex">
        <Menu as="div">
          <div
            projectid={project.id}
            onClick={(e) => {
              setprojectid(e.target.projectid);
            }}
          >
            <Menu.Button
              className={`${
                projectid === project.id ? "bg-blue-900" : "bg-blue-600"
              } optionsbtn bg-green-500 px-1 py-0.5 hover:bg-green-300 rounded`}
            >
              <i className="fas fa-cog text-white pt-0.5"></i>
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="absolute z-50 right-8 w-54 mt-2 -translate-x-7 -translate-y-52 origin-top-left bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div className="px-1 py-1 ">
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onEditProjectClick}
                    >
                      <i className="fas fa-pen text-xs mr-1 w-4"></i> Edit
                      Project
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={addModuleClick}
                    >
                      <i className="fas fa-file text-xs mr-1 w-4"></i>
                      {" Modules"}
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onEstimateClick}
                    >
                      <i className="fas fa-file-alt text-xs mr-1 w-4"></i>{" "}
                      Project Estimation
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onGovernanceClick}
                    >
                      <i className="fas fa-landmark text-xs mr-1 w-4"></i>{" "}
                      Governance
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onTestdesignClick}
                    >
                      <i className="fas fa-ruler-combined text-xs mr-1 w-4"></i>{" "}
                      Test Design Progress
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onTestexecutionClick}
                    >
                      <i className="fas fa-pencil-ruler text-xs mr-1 w-4"></i>{" "}
                      Test Execution Progress
                    </button>
                  )}
                </Menu.Item>

                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onALMClick}
                    >
                      <i className="fas fa-project-diagram text-xs mr-1 w-4"></i>{" "}
                      ALM Settings
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onJIRAClick}
                    >
                      <i className="fas fa-cog text-xs mr-1 w-4"></i> JIRA
                      Settings
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onSyncStartEndClick}
                    >
                      <i className="fas fa-clock text-xs mr-1 w-4"></i> Sync
                      Start / End Date
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 border-b dark:text-gray-200 dark:border-gray-500 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onSummaryClick}
                    >
                      <i className="fas fa-book text-xs mr-1 w-4"></i> Summary
                    </button>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={
                        "block w-full text-left px-2 py-2 cursor-pointer text-md text-gray-800 dark:text-gray-200 hover:bg-blue-100 dark:hover:bg-gray-600"
                      }
                      onClick={onEffortClick}
                    >
                      <i className="fas fa-book text-xs mr-1 w-4"></i> Effort
                    </button>
                  )}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      </ul>
    </>
  );
};

export default Projectoptions;
