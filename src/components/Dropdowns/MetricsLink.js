import React from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
const MetricsLink = ({ rowvalue, onMetricsClick }) => {
  const onMetricsGenarateClick = () => {
    onMetricsClick(rowvalue);
  };

  return (
    <>
      <ul className="flex-col md:flex-row list-none items-center hidden md:flex">
        <Menu as="div">
          <div>
            <Menu.Button className="optionsbtn bg-rose-500 px-1 py-0.5 hover:bg-rose-300 rounded">
              <span className="tooltiptext">Metrics Page</span>
              <i
                className="fas fa-chart-area text-white pt-0.5 "
                onClick={onMetricsGenarateClick}
              ></i>
            </Menu.Button>
          </div>
        </Menu>
      </ul>
    </>
  );
};

export default MetricsLink;
