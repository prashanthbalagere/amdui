import React, { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { BrowserRouter as Link } from "react-router-dom";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { useHistory } from "react-router";
import { SERVER_URL } from "../../constants";
const token = sessionStorage.getItem("jwt");

const ELKDashboard = () => {
  let [isOpen, setIsOpen] = useState(false);
  let [notes, setNotes] = useState([
    { id: 1, name: "Notes 1", value: "Notes written here", category: "none" },
    { id: 2, name: "Notes 2", value: "Notes written here", category: "none" },
    { id: 3, name: "Notes 3", value: "Notes written here", category: "none" },
  ]);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };
  const newNote = {
    id: Math.floor(Math.random() * 90000) + 10000,
    name: "Notes",
    value: "Notes written here",
    category: "none",
  };
  const addNote = () => {
    setNotes([...notes, newNote]);
  };
  useEffect(() => {
    getDashboardUrls(sessionStorage.getItem("projectID"));
  }, []);
  const getDashboardUrls = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/dashboardUrls", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.DashboardUrls;
        setUrlData(datares);
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  const [openTab, setOpenTab] = React.useState(1);
  const [urlData, setUrlData] = React.useState([]);
  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">ELK Dashboard</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button onClick={openModal} className="default-btn" type="button">
              <i className="fas fa-book mr-1 text-md text-white"></i> Notes
            </button>
            <button onClick={handleClose} className="default-btn" type="button">
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap">
        {urlData.length === 0 ? (
          <div className="flex flex-wrap w-full bg-white p-4 px-1 rounded-lg shadow-lg">
            <div className="w-full lg:w-4/12 px-4 text-sm">
              Please add dashboard URL's.
            </div>
          </div>
        ) : null}
        <ul className="flex mb-0 list-none pb-1 pl-1 flex-row" role="tablist">
          {urlData.map((elm) => {
            return (
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <a
                  className={
                    "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                    (openTab === elm.id
                      ? "text-white bg-blue-800"
                      : "text-blue-800 bg-gray-50")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenTab(elm.id);
                  }}
                  data-toggle="tab"
                  href="#link"
                  role="tablist"
                >
                  <i className="fas fa-chart-area text-base mr-1"></i>{" "}
                  {elm.name}
                </a>
              </li>
            );
          })}
        </ul>

        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 mt-2">
          <div className=" py-2 flex-auto">
            <div className="tab-content tab-space">
              {urlData.map((elm) => {
                return (
                  <div
                    className={openTab === elm.id ? "block" : "hidden"}
                    id="link1"
                  >
                    <div className="flex flex-wrap">
                      <iframe
                        title={elm.name}
                        width="1400"
                        height="700"
                        src={elm.value}
                      ></iframe>
                    </div>
                  </div>
                );
              })}

              {/* <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                <div className="flex flex-wrap">
                  <iframe
                    title="Test Design Graph"
                    width="1400"
                    height="700"
                    src="http://34.125.173.23:5601/app/dashboards#/view/59277910-00c1-11ec-bdd5-5337f7a073d4?embed=true&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))&hide-filter-bar=true"
                  ></iframe>
                </div>
              </div>
              <div className={openTab === 2 ? "block" : "hidden"} id="link1">
                <div className="flex flex-wrap">
                  <iframe
                    title="ELKDashboard - Test Design Graph"
                    width="1400"
                    height="700"
                    src="http://34.125.173.23:5601/app/dashboards#/view/c5a68410-002f-11ec-a331-c39b26f3d059?embed=true&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))&hide-filter-bar=true"
                  ></iframe>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto bg-black bg-opacity-50"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full p-6 my-8 mt-44 overflow-hidden text-left transition-all transform bg-white shadow-xl rounded-xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  Notes
                </Dialog.Title>

                <div className="mt-2">
                  <table className="moduletable min-w-full leading-normal">
                    <thead>
                      <tr>
                        <th className=" w-52 px-5 py-3 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                          Name
                        </th>
                        <th className="px-5 py-3 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                          Description
                        </th>
                        <th className="px-5 py-3 w-5 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                      </tr>
                    </thead>
                    {notes.length > 0 ? (
                      <tbody>
                        {notes.map((elm) => {
                          return (
                            <tr key={elm.id}>
                              <td>
                                <input
                                  type="text"
                                  name="name"
                                  className="metrics-input-notes"
                                  defaultValue={elm.name}
                                  // onChange={(e) =>
                                  //   this.handleChangeNameChange(e, elm.id)
                                  // }
                                />
                              </td>
                              <td>
                                <input
                                  type="text"
                                  name="value"
                                  className="metrics-input-notes"
                                  defaultValue={elm.value}
                                  // onChange={(e) =>
                                  //   this.handleChangeValueChange(e, elm.id)
                                  // }
                                />
                              </td>

                              <td className="text-center cursor-pointer">
                                <i
                                  className="fas fa-times mr-1 p-1 px-2 ml-3 text-md rounded bg-gray-100 hover:bg-gray-300"
                                  onClick={() => {
                                    this.onDeleteRow(elm.id);
                                  }}
                                ></i>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    ) : (
                      <tbody>
                        <tr>
                          <td colSpan="3" className="text-center py-3 text-sm">
                            No dashboard URL's added
                          </td>
                        </tr>
                      </tbody>
                    )}
                  </table>
                </div>

                <div className="mt-4 text-center">
                  <button
                    className="default-btn "
                    type="button"
                    onClick={addNote}
                  >
                    <i className="fas fa-plus text-md text-white"></i> Add Note
                  </button>

                  <button
                    type="button"
                    className="default-btn"
                    onClick={closeModal}
                  >
                    Save
                  </button>
                  <button
                    className="default-btn-cancel"
                    onClick={closeModal}
                    type="button"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default ELKDashboard;
