import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { SERVER_URL } from "../../constants.js";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";

import "react-tagsinput/react-tagsinput.css";
const token = sessionStorage.getItem("jwt");

class ELKurls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectid: "",
      data: [],
    };
  }

  componentDidMount() {
    this.fetchProject();
  }

  updateSettings = (projectId) => {
    let sendval = this.state.data;
    fetch(SERVER_URL + "projects/" + projectId + "/dashboardUrls", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(sendval),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };
  fetchProject = () => {
    this.setState({
      projectid: sessionStorage.getItem("projectID"),
    });
    this.getDashboardUrls(sessionStorage.getItem("projectID"));
  };

  getDashboardUrls = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/dashboardUrls", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.DashboardUrls;
        this.setState({
          data: datares,
        });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  addURL = () => {
    this.setState({
      data: this.state.data.concat({
        category: "none",
        id: Math.floor(Math.random() * 90000) + 10000,
        name: "name",
        type: "URL",
        value: "https://",
      }),
    });
  };
  onDeleteRow = (id) => {
    const items = this.state.data.filter((item) => item.id !== id);
    this.setState({ data: items });
  };
  handleChangeNameChange = (e, id) => {
    const item = [...this.state.data];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.name = e.target.value;
      }
    });
    this.setState({ data: item });
  };
  handleChangeValueChange = (e, id) => {
    const item = [...this.state.data];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.value = e.target.value;
      }
    });
    this.setState({ data: item });
  };
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { projectid, data } = this.state;
    return (
      <>
        <div className=" mb-0 py-4">
          <div className="flex flex-row">
            <div className="w-full xl:w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">ELK Dashboard URL's</h6>
            </div>
            <div className="w-full xl:w-6/12 text-right">
              <button
                className="default-btn "
                type="button"
                onClick={() => {
                  this.addURL();
                }}
              >
                <i className="fas fa-plus text-md text-white"></i> Add URL
              </button>

              <button
                onClick={this.handleClose}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-times mr-1 text-md text-white"></i> Close
              </button>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className="w-full">
            <form>
              <div className="flex flex-wrap">
                <div className="w-full lg:w-12/12 ">
                  <div className="inline-block min-w-full overflow-hidden bg-white p-3  rounded-lg shadow-lg">
                    <table className="moduletable min-w-full leading-normal">
                      <thead>
                        <tr>
                          <th className=" w-52 px-5 py-3 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Name
                          </th>
                          <th className="px-5 py-3 w-5 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Type
                          </th>
                          <th className="px-5 py-3 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Value
                          </th>
                          <th className="px-5 py-3 w-5 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                        </tr>
                      </thead>
                      {data.length > 0 ? (
                        <tbody>
                          {data.map((elm) => {
                            return (
                              <tr key={elm.id}>
                                <td>
                                  <input
                                    type="text"
                                    name="name"
                                    className="metrics-input-addmodules"
                                    defaultValue={elm.name}
                                    onChange={(e) =>
                                      this.handleChangeNameChange(e, elm.id)
                                    }
                                  />
                                </td>
                                <td>
                                  <input
                                    type="text"
                                    name="type"
                                    className="metrics-input-addmodules"
                                    defaultValue={elm.type}
                                    disabled
                                    onChange={this.handleChange}
                                  />
                                </td>
                                <td>
                                  <input
                                    type="text"
                                    name="value"
                                    className="metrics-input-addmodules"
                                    defaultValue={elm.value}
                                    onChange={(e) =>
                                      this.handleChangeValueChange(e, elm.id)
                                    }
                                  />
                                </td>

                                <td className="text-center cursor-pointer">
                                  <i
                                    className="fas fa-times mr-1 p-1 px-2 ml-3 text-md rounded bg-gray-100 hover:bg-gray-300"
                                    onClick={() => {
                                      this.onDeleteRow(elm.id);
                                    }}
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      ) : (
                        <tbody>
                          <tr>
                            <td
                              colSpan="3"
                              className="text-center py-3 text-sm"
                            >
                              No dashboard URL's added
                            </td>
                          </tr>
                        </tbody>
                      )}
                    </table>
                  </div>
                </div>
              </div>
              {data.length > 0 ? (
                <div className="flex flex-col mt-3">
                  <div className="lg:w-12/12 xl:w-12/12 px-2">
                    <div className="flex flex-wrap justify-center py-2">
                      <button
                        className="default-btn"
                        type="button"
                        onClick={() => {
                          this.updateSettings(projectid);
                        }}
                      >
                        Save
                      </button>
                      <Link to="/dashboard">
                        <button
                          className="default-btn-cancel"
                          onClick={this.handleClose}
                          type="button"
                        >
                          Cancel
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              ) : null}
            </form>
          </div>
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default ELKurls;
