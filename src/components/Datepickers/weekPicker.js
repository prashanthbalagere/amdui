import React from "react";
import Helmet from "react-helmet";
import moment from "moment";
import DayPicker from "react-day-picker";
import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import "react-day-picker/lib/style.css";

function getWeekDays(weekStart) {
  const days = [weekStart];
  for (let i = 1; i < 7; i += 1) {
    days.push(moment(weekStart).add(i, "days").toDate());
  }
  return days;
}

function getWeekRange(date) {
  return {
    from: moment(date).startOf("week").toDate(),
    to: moment(date).endOf("week").toDate(),
  };
}

export default class Weekpicker extends React.Component {
  state = {
    hoverRange: undefined,
    selectedDays: [],
    selectedWeek: "Week-01",
    selectedWeekNumber: "01",
    selectedStartDate: "",
    selectedEndDate: "",
  };

  handleDayChange = (date, e) => {
    this.setState(
      {
        selectedDays: getWeekDays(getWeekRange(date).from),
        selectedWeek: `Week-${moment(date).week() - 1}`,
        selectedWeekNumber: `${moment(date).week() - 1}`,
        selectedStartDate: moment(
          getWeekDays(getWeekRange(date).from)[0]
        ).format("DD/MM/yyyy"),
        selectedEndDate: moment(getWeekDays(getWeekRange(date).from)[6]).format(
          "DD/MM/yyyy"
        ),
      },
      () => {
        this.props.weekChange(this.state.selectedWeek);
      }
    );
  };
  descreaseWeek = () => {
    this.setState(
      {
        selectedWeek: `Week-${+this.state.selectedWeekNumber - 1}`,
        selectedWeekNumber: `${+this.state.selectedWeekNumber - 1}`,
        selectedStartDate: this.removeDays(this.state.selectedStartDate),
        selectedEndDate: this.removeDays(this.state.selectedEndDate),
      },
      () => {
        this.props.weekChange(`Week-${+this.state.selectedWeekNumber}`);
      }
    );
  };
  increaseWeek = () => {
    this.setState(
      {
        selectedWeek: `Week-${+this.state.selectedWeekNumber + 1}`,
        selectedWeekNumber: `${+this.state.selectedWeekNumber + 1}`,
        selectedStartDate: this.addDays(this.state.selectedStartDate),
        selectedEndDate: this.addDays(this.state.selectedEndDate),
      },
      () => {
        this.props.weekChange(`Week-${+this.state.selectedWeekNumber}`);
      }
    );
  };
  addDays = (dateval) => {
    let a = dateval.split(/\//);
    let newdate = [a[1], a[0], a[2]].join("-");
    let b = moment(newdate).toDate();
    var date = new Date(b);
    date.setDate(date.getDate() + 7);
    return moment(date).format("DD/MM/yyyy");
  };
  removeDays = (dateval) => {
    let a = dateval.split(/\//);
    let newdate = [a[1], a[0], a[2]].join("-");
    let b = moment(newdate).toDate();
    var date = new Date(b);
    date.setDate(date.getDate() - 7);
    return moment(date).format("DD/MM/yyyy");
  };
  handleDayEnter = (date) => {
    this.setState({
      hoverRange: getWeekRange(date),
    });
  };

  handleDayLeave = () => {
    this.setState({
      hoverRange: undefined,
    });
  };

  handleWeekClick = (weekNumber, days, e) => {
    this.setState({
      selectedDays: days,
    });
  };
  componentDidMount() {
    this.handleDayChange(moment(), 0);
  }
  render() {
    const {
      hoverRange,
      selectedDays,
      selectedWeek,
      selectedStartDate,
      selectedEndDate,
    } = this.state;
    const daysAreSelected = selectedDays.length > 0;
    const modifiers = {
      hoverRange,
      selectedRange: daysAreSelected && {
        from: selectedDays[0],
        to: selectedDays[6],
      },
      hoverRangeStart: hoverRange && hoverRange.from,
      hoverRangeEnd: hoverRange && hoverRange.to,
      selectedRangeStart: daysAreSelected && selectedDays[0],
      selectedRangeEnd: daysAreSelected && selectedDays[6],
    };

    return (
      <React.Fragment>
        <Menu as="div" className="relative inline-block text-left">
          {({ open }) => (
            <>
              <p className="text-xs font-semibold text-gray-600">
                Select a Week
              </p>
              <div>
                <Menu.Button className="inline-flex w-24 rounded-md border border-gray-300 shadow-sm px-4 py-2 mb-3 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                  {selectedWeek}
                </Menu.Button>
                <Menu.Button className="inline-flex w-48 rounded-md border border-gray-300 shadow-sm px-3 py-2 ml-2 mb-3 bg-gray-200 text-sm font-medium text-gray-700 cursor-default ">
                  {selectedStartDate} - {selectedEndDate}
                </Menu.Button>
                <button
                  className={"default-btn-arrows ml-2"}
                  onClick={this.descreaseWeek}
                >
                  <i className="fas fa-caret-left text-lg mr-1 w-4"></i>
                </button>
                <button
                  className={"default-btn-arrows"}
                  onClick={this.increaseWeek}
                >
                  <i className="fas fa-caret-right text-lg ml-1 w-4"></i>
                </button>
              </div>

              <Transition
                show={open}
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items
                  static
                  className="origin-top-left absolute mt-2 w-100 z-50 top-12 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                >
                  <div className="py-1">
                    <div className="SelectedWeekExample">
                      <DayPicker
                        selectedDays={selectedDays}
                        showWeekNumbers
                        showOutsideDays
                        modifiers={modifiers}
                        onDayClick={this.handleDayChange}
                        onDayMouseEnter={this.handleDayEnter}
                        onDayMouseLeave={this.handleDayLeave}
                        onWeekClick={this.handleWeekClick}
                      />
                      <Helmet>
                        <style>{`
                        .SelectedWeekExample .DayPicker-Month {
                          border-collapse: separate;
                        }
                        .SelectedWeekExample .DayPicker-WeekNumber {
                          outline: none;
                        }
                        .SelectedWeekExample .DayPicker-Day {
                          outline: none;
                          border: 1px solid transparent;
                        }
                        .SelectedWeekExample .DayPicker-Day--hoverRange {
                          background-color: #EFEFEF !important;
                        }

                        .SelectedWeekExample .DayPicker-Day--selectedRange {
                          background-color: #fff7ba !important;
                          border-top-color: #FFEB3B;
                          border-bottom-color: #FFEB3B;
                          border-left-color: #fff7ba;
                          border-right-color: #fff7ba;
                        }

                        .SelectedWeekExample .DayPicker-Day--selectedRangeStart {
                          background-color: #FFEB3B !important;
                          border-left: 1px solid #FFEB3B;
                        }

                        .SelectedWeekExample .DayPicker-Day--selectedRangeEnd {
                          background-color: #FFEB3B !important;
                          border-right: 1px solid #FFEB3B;
                        }

                        .SelectedWeekExample .DayPicker-Day--selectedRange:not(.DayPicker-Day--outside).DayPicker-Day--selected,
                        .SelectedWeekExample .DayPicker-Day--hoverRange:not(.DayPicker-Day--outside).DayPicker-Day--selected {
                          border-radius: 0 !important;
                          color: black !important;
                        }
                        .SelectedWeekExample .DayPicker-Day--hoverRange:hover {
                          border-radius: 0 !important;
                        }
                      `}</style>
                      </Helmet>
                    </div>
                  </div>
                </Menu.Items>
              </Transition>
            </>
          )}
        </Menu>
      </React.Fragment>
    );
  }
}
