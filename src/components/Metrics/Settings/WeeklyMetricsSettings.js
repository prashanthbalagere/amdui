import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import { ToastContainer, toast } from "react-toastify";
import VerticaltabsMetrics from "../verticalTabsMetrics.js";
const token = sessionStorage.getItem("jwt");

class WeeklyMetricsHeatMapSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      estimates: [],
      selectedNumber: null,
      hide: false,
    };
  }

  componentDidMount() {
    this.fetchAllModules("None");
  }
  // Update estimates
  updateEstimates(projectId, updatedEstimates) {
    console.log(JSON.stringify(updatedEstimates));

    fetch(
      SERVER_URL + "projects/" + projectId + "/weeklyMetricsHeatMapSettings",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify(updatedEstimates),
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }
  fetchAllModules = (month) => {
    this.getAllModules(sessionStorage.getItem("projectID"), month);
  };

  getAllModules = (projectId, month) => {
    fetch(
      SERVER_URL + "projects/" + projectId + "/weeklyMetricsHeatMapSettings",
      {
        method: "GET",
        headers: {
          Authorization: token,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        console.log(
          "The modules response from the server is : " + responseData
        );

        this.setState({
          estimates: responseData.MetricsSettings,
        });
      })
      .catch((err) => console.error(err));
  };

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { selectedNumber, estimates } = this.state;

    return (
      <div className="App3">
        {estimates.length > 0 ? (
          <VerticaltabsMetrics
            tabsdata={estimates}
            triggertoSave={this.handleSave}
          />
        ) : null}
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  // Save project and close modal form
  handleSave = () => {
    this.updateEstimates(
      sessionStorage.getItem("projectID"),
      this.state.estimates
    );
  };
}
export default WeeklyMetricsHeatMapSettings;
