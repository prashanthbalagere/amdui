import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
import VerticalTabsEstimate from "../VerticalTabsEstimate.js";
const token = sessionStorage.getItem("jwt");
class ProjectEstimate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      estimates: [],
      baselineEstimates: [],
      hide: false,
    };
  }

  componentDidMount() {
    this.fetchAllModules();
  }

  fetchAllModules = () => {
    this.getAllEstimates(sessionStorage.getItem("projectID"));
    this.getAllbaselineEstimates(sessionStorage.getItem("projectID"));
  };

  getAllEstimates = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/estimates", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        loaderHide();

        this.setState({
          estimates: responseData.estimates,
        });
      })
      .catch((err) => console.error(err));
  };
  getAllbaselineEstimates = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/baselineEstimates", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        loaderHide();

        this.setState({
          baselineEstimates: responseData.estimates,
        });
      })
      .catch((err) => console.error(err));
  };

  // Update estimates
  updateEstimates(projectId, updatedEstimates) {
    fetch(SERVER_URL + "projects/" + projectId + "/estimates", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(updatedEstimates),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { estimates, baselineEstimates } = this.state;
    return (
      <div>
        <div>
          <div className="">
            <div className="flex flex-col">
              {estimates.length > 0 ? (
                <VerticalTabsEstimate
                  estimatesdata={estimates}
                  baseestimatesdata={baselineEstimates}
                  triggertoSave={this.handleSave}
                />
              ) : (
                <p className="text-center mt-5">
                  Modules missing. Please add modules before you estimate the
                  project.
                </p>
              )}
            </div>
          </div>
        </div>
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  // Save project and close modal form
  handleSave = () => {
    if (
      this.state.estimates.find((x) => x.name === "Planned Start Date")
        .value === "" ||
      this.state.estimates.find((x) => x.name === "Planned End Date").value ===
        ""
    ) {
      toast.error("Enter valid dates", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    } else {
      this.updateEstimates(
        sessionStorage.getItem("projectID"),
        this.state.estimates
      );
    }
  };
}

export default ProjectEstimate;
