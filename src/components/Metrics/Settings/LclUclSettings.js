import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import { ToastContainer, toast } from "react-toastify";
import VerticaltabsMetrics from "../verticalTabsMetrics.js";
const token = sessionStorage.getItem("jwt");

class LclUclSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      estimates: [],
      selectedNumber: null,
      hide: false,
      newdata: [],
    };
  }

  componentDidMount() {
    this.fetchAllModules("None");
  }
  // Update estimates
  updateEstimates(projectId, updatedEstimates) {
    fetch(SERVER_URL + "projects/" + projectId + "/updateHeatMapSettings", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(updatedEstimates),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }
  fetchAllModules = (month) => {
    this.getAllModules(sessionStorage.getItem("projectID"), month);
  };

  async getAllModules(projectId, month) {
    await fetch(SERVER_URL + "projects/" + projectId + "/heatMapSettings", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          estimates: responseData.MetricsSettings,
        });
      })
      .catch((err) => console.error(err));
  }

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { estimates } = this.state;

    return (
      <div className="App3">
        {estimates.length > 0 ? (
          <VerticaltabsMetrics
            tabsdata={estimates}
            triggertoSave={this.handleSave}
          />
        ) : null}
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  // Save project and close modal form
  handleSave = () => {
    this.updateEstimates(
      sessionStorage.getItem("projectID"),
      this.state.estimates
    );
  };
}
export default LclUclSettings;
