import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

class VerticalTabsEstimate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estimatesnew: props.estimatesdata,
      baselinenew: props.baseestimatesdata,
      unique: [],
      newTab: 0,
    };
  }
  componentDidMount() {
    this.genarateUnique();
  }
  handleChange = (e) => {
    var data = [...this.state.estimatesnew];
    var index = data.findIndex(
      (obj) => obj.id === +e.target.getAttribute("id")
    );
    data[index].value = e.target.value;
    this.setState({ data });
  };
  handleChangeEstimateDate = (e, date, id) => {
    var newdate = "";
    if (e === null) {
      newdate = "";
    } else {
      newdate = moment(e).format("DD-MM-yyyy");
    }
    var data = [...this.state.estimatesnew];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].value = newdate;
    this.setState({ data });
  };
  genarateUnique = () => {
    let unique = [
      ...new Set(this.state.estimatesnew.map((item) => item.category)),
    ];
    this.setState({
      unique: unique,
    });
  };
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };
  handleSave = () => {
    this.props.triggertoSave(this.state.estimatesnew);
  };
  componentWillReceiveProps(nextProps) {
    this.setState({ estimatesnew: nextProps.estimatesdata });
    this.setState({ baselinenew: nextProps.baseestimatesdata });
  }
  render() {
    const { estimatesnew, baselinenew, newTab, unique } = this.state;
    return (
      <div>
        <div className="flex px-1 py-1">
          <div className="flex px-4 py-3 bg-white rounded-lg shadow-lg">
            <ul
              className="flex-grow w-64 relative mb-0 list-none pb-4 flex-row "
              role="tablist"
            >
              {unique.map((item, index) => (
                <li
                  className="my-1 w-100 flex-auto text-left"
                  key={`${index}-${item}`}
                >
                  <a
                    className={
                      "text-xs font-bold capitalize px-4 py-2 block leading-normal " +
                      (newTab === index
                        ? "text-white bg-blue-800 "
                        : "text-gray-900 bg-blue-100 hover:bg-blue-800 hover:text-white")
                    }
                    onClick={(e) => {
                      e.preventDefault();
                      this.setState({
                        newTab: index,
                      });
                    }}
                    data-toggle="tab"
                    href={`#tab${index}`}
                    role="tablist"
                    key={index}
                  >
                    {item}
                  </a>
                </li>
              ))}
            </ul>
          </div>
          <div className="relative  min-w-0 break-words w-full px-4 py-3  ml-3 bg-white rounded-lg shadow-lg">
            <div className="">
              <div className="tab-content tab-space">
                {unique.map((item, index) => (
                  <div
                    className={newTab === index ? "block" : "hidden"}
                    id={`tab${index}`}
                    key={index}
                  >
                    <table className="items-center w-3/4 bg-white border-collapse my-1 inline-table float-left">
                      <thead>
                        <tr>
                          {/* <th
                            className={
                              "px-6 w-10 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          ></th> */}
                          <th
                            className={
                              "px-6 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Input
                          </th>

                          <th
                            className={
                              "px-6 w-56 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Estimate
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {estimatesnew.map((elm) => {
                          if (elm.category === item) {
                            return (
                              <tr key={`${elm.id}-${elm.value}`}>
                                {/* <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap">
                                  {elm.id}
                                </td> */}
                                <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap">
                                  {elm.name}
                                </td>

                                {elm.type === "number" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <input
                                      type="number"
                                      min="0"
                                      defaultValue={elm.value}
                                      id={elm.id}
                                      onBlur={this.handleChange}
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                    />
                                  </td>
                                ) : null}
                                {elm.type === "date" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <DatePicker
                                      id={elm.id}
                                      selected={
                                        elm.value === "" || elm.value === "null"
                                          ? null
                                          : new Date(
                                              moment(
                                                elm.value,
                                                "DD-MM-YYYY"
                                              ).format("MM-DD-YYYY")
                                            )
                                      }
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                      dateFormat="dd/MM/yyyy"
                                      onChange={(e, date) => {
                                        var id = elm.id;
                                        this.handleChangeEstimateDate(
                                          e,
                                          date,
                                          id
                                        );
                                      }}
                                      autoComplete="off"
                                    />
                                    <br></br>
                                    {elm.value === "" ? (
                                      <span className="text-rose-700 float-right">
                                        please enter a valid date
                                      </span>
                                    ) : null}
                                  </td>
                                ) : null}
                              </tr>
                            );
                          }
                        })}
                      </tbody>
                    </table>
                    <table className="items-center w-1/4 float-left bg-white border-collapse my-1 inline-table">
                      <thead>
                        <tr>
                          <th
                            className={
                              "px-6 w-56 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Baseline
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {baselinenew.map((elm) => {
                          if (elm.category === item) {
                            return (
                              <tr key={`${elm.id}-${elm.value}`}>
                                {elm.type === "number" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <input
                                      type="number"
                                      min="0"
                                      defaultValue={elm.value}
                                      disabled={elm.edit}
                                      id={elm.id}
                                      onBlur={this.handleChange}
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0 disabled:bg-blue-100 disabled:cursor-not-allowed"
                                    />
                                  </td>
                                ) : null}
                                {elm.type === "date" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <DatePicker
                                      id={elm.id}
                                      selected={
                                        elm.value === ""
                                          ? null
                                          : new Date(
                                              moment(
                                                elm.value,
                                                "DD-MM-YYYY"
                                              ).format("MM-DD-YYYY")
                                            )
                                      }
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0 disabled:bg-blue-100 disabled:cursor-not-allowed"
                                      dateFormat="dd/MM/yyyy"
                                      disabled={elm.edit}
                                      onChange={(e, date) => {
                                        var id = elm.id;
                                        this.handleChangeEstimateDate(
                                          e,
                                          date,
                                          id
                                        );
                                      }}
                                      autoComplete="off"
                                    />
                                  </td>
                                ) : null}
                              </tr>
                            );
                          }
                        })}
                      </tbody>
                    </table>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="lg:w-12/12 xl:w-12/12 px-2">
            <div className="flex flex-wrap justify-center py-4">
              <button
                className="default-btn"
                type="button"
                onClick={this.handleSave}
              >
                Save
              </button>
              <Link to="/dashboard">
                <button className="default-btn-cancel" type="button">
                  Cancel
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VerticalTabsEstimate;
