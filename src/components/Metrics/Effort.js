import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { SERVER_URL } from "../../constants.js";
import DatepickerFlat from "./Datepickerflat.js";
import Flatpickr from "react-flatpickr";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
// import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import "react-tagsinput/react-tagsinput.css";
const token = sessionStorage.getItem("jwt");

class Effort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectid: "",
      ddates: {},
      effort: [],
      types: [],
    };
  }

  componentDidMount() {
    this.fetchEffort();
  }

  updateSettings = (projectId) => {
    let sendval = this.state.effort;
    fetch(SERVER_URL + "projects/" + projectId + "/updateEfforts", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(sendval),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };
  fetchEffort = () => {
    this.setState({
      projectid: sessionStorage.getItem("projectID"),
    });
    this.getEffortSettings(sessionStorage.getItem("projectID"));
    this.getEfforts(sessionStorage.getItem("projectID"));
  };

  getEffortSettings = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/getEffortSettings", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.types;
        this.setState({
          types: datares,
        });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  getEfforts = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/getEffortDetails", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.efforts;
        this.setState({
          effort: datares,
        });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  addEffort = () => {
    this.setState({
      effort: this.state.effort.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        name: this.state.types[0].id,
        startDate: null,
        endDate: null,
        numberOfResources: "",
      }),
    });
  };
  onDeleteRow = (id) => {
    const tobedeleted = this.state.effort.filter((item) => item.id === id);

    fetch(SERVER_URL + "projects/" + this.state.projectid + "/deleteEfforts", {
      method: "POST",
      body: JSON.stringify(tobedeleted),
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          const items = this.state.effort.filter((item) => item.id == id);
          this.setState({ effort: items });
          this.collectDisabledDates(items);
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };
  HandleTypeChange = (e, id) => {
    const item = [...this.state.effort];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.name = e.target.value;
      }
    });
    this.setState({ effort: item });
  };
  handleChangeResourceChange = (e, id) => {
    const item = [...this.state.effort];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.numberOfResources = e.target.value;
      }
    });
    this.setState({ effort: item });
  };

  handleChangeDate = (date, id) => {
    console.log(`date`, date);
    var data = [...this.state.effort];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].startDate = moment(date[0]).format("DD-MM-YYYY");
    data[index].endDate = moment(date[1]).format("DD-MM-YYYY");
    this.setState({ data });
    this.collectDisabledDates(data);
  };

  findDisabledValues = (t, i) => {
    return i
      .filter((obj) => obj.name.includes(t))
      .map((item) => ({
        from: item.startDate,
        to: item.endDate,
      }));
  };
  collectDisabledDates = (items) => {
    const result = items.reduce(
      (obj, cur) => ({
        ...obj,
        [cur.name]: this.findDisabledValues(cur.name, items),
      }),
      {}
    );
    console.log(`result`, result);
    this.setState({ ddates: result });

    // const collecteddates = items.map((item) => {
    //   return {
    //     from: item.startDate,
    //     to: item.endDate,
    //   };
    // });
  };
  // handleChangestartDate = (e, date, id) => {
  //   var newdate = "";
  //   if (e === null) {
  //     newdate = "";
  //   } else {
  //     newdate = moment(e).format("DD-MM-yyyy");
  //   }
  //   var data = [...this.state.effort];
  //   var index = data.findIndex((obj) => obj.id === id);
  //   data[index].startDate = newdate;
  //   this.setState({ data });
  // };

  // handleChangeendDate = (e, date, id) => {
  //   var newdate = "";
  //   if (e === null) {
  //     newdate = "";
  //   } else {
  //     newdate = moment(e).format("DD-MM-yyyy");
  //   }
  //   var data = [...this.state.effort];
  //   var index = data.findIndex((obj) => obj.id === id);
  //   data[index].endDate = newdate;
  //   this.setState({ data });
  // };
  // setDateRange = (dates) => {
  //   console.log(`dates`, dates);
  // };
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { projectid, effort, types, ddates } = this.state;
    return (
      <>
        <div className="flex  justify-center mb-0 py-4">
          <div className="flex flex-row  w-8/12">
            <div className="w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">Effort</h6>
            </div>
            <div className="w-6/12 text-right">
              <button
                className="default-btn "
                type="button"
                onClick={() => {
                  this.addEffort();
                }}
              >
                <i className="fas fa-plus text-md text-white"></i> Add Effort
              </button>

              <button
                onClick={this.handleClose}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-times mr-1 text-md text-white"></i> Close
              </button>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className=" w-8/12">
            <form>
              <div className="flex flex-wrap">
                <div className="w-full">
                  <div className="inline-block min-w-full bg-white p-3  rounded-lg shadow-lg">
                    <table className="moduletable min-w-full leading-normal">
                      <thead>
                        <tr>
                          <th className=" w-80 px-5 py-3 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Type
                          </th>
                          <th className="px-5 py-3 w-40 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Date
                          </th>

                          <th className="px-5 py-3  w-8 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                            Number of Resources
                          </th>
                          <th className="px-5 py-3 w-5 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                        </tr>
                      </thead>
                      {effort.length > 0 ? (
                        <tbody>
                          {effort.map((elm) => {
                            return (
                              <tr key={elm.id}>
                                <td>
                                  <select
                                    className="mx-0.5 mt-0.5 text-left px-1 py-1.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                    name="type"
                                    defaultValue={elm.name}
                                    onChange={(e) => {
                                      this.HandleTypeChange(e, elm.id);
                                    }}
                                  >
                                    {types.map((item) => {
                                      return (
                                        <option value={item.id}>
                                          {item.name}
                                        </option>
                                      );
                                    })}
                                  </select>
                                </td>
                                <td>
                                  {" "}
                                  <div className="relative  w-full">
                                    <Flatpickr
                                      className="form-input pl-9 text-gray-800 font-normal text-sm w-full border border-blue-100 rounded mx-0.5 mt-0.5 px-1 py-1.5"
                                      options={{
                                        mode: "range",
                                        static: true,
                                        monthSelectorType: "static",
                                        dateFormat: "d/m/Y",
                                        defaultDate: [
                                          elm.startDate,
                                          elm.endDate,
                                        ],
                                        prevArrow:
                                          '<svg className="fill-current" width="7" height="11" viewBox="0 0 7 11"><path d="M5.4 10.8l1.4-1.4-4-4 4-4L5.4 0 0 5.4z" /></svg>',
                                        nextArrow:
                                          '<svg className="fill-current" width="7" height="11" viewBox="0 0 7 11"><path d="M1.4 10.8L0 9.4l4-4-4-4L1.4 0l5.4 5.4z" /></svg>',
                                        onReady: (
                                          selectedDates,
                                          dateStr,
                                          instance
                                        ) => {
                                          instance.element.value =
                                            dateStr.replace("to", "-");
                                        },
                                        onChange: (
                                          selectedDates,
                                          dateStr,
                                          instance
                                        ) => {
                                          instance.element.value =
                                            dateStr.replace("to", "-");
                                        },
                                        disable:
                                          ddates[elm.name] === undefined
                                            ? []
                                            : ddates[elm.name],
                                      }}
                                      onChange={(date, dateStr) => {
                                        if (date.length === 2) {
                                          this.handleChangeDate(date, elm.id);
                                        }
                                      }}
                                    />
                                    <div className="absolute inset-0 right-auto flex items-center pointer-events-none">
                                      <svg
                                        className="w-4 h-4 fill-current text-gray-500 ml-3"
                                        viewBox="0 0 16 16"
                                      >
                                        <path d="M15 2h-2V0h-2v2H9V0H7v2H5V0H3v2H1a1 1 0 00-1 1v12a1 1 0 001 1h14a1 1 0 001-1V3a1 1 0 00-1-1zm-1 12H2V6h12v8z" />
                                      </svg>
                                    </div>
                                  </div>
                                </td>

                                <td>
                                  <input
                                    type="text"
                                    name="value"
                                    className="mx-0.5 mt-0.5 text-right px-1 py-1.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                    defaultValue={elm.numberOfResources}
                                    onChange={(e) =>
                                      this.handleChangeResourceChange(e, elm.id)
                                    }
                                  />
                                </td>

                                <td className="text-center cursor-pointer">
                                  <i
                                    className="fas fa-times mr-1 p-1 px-2 ml-3 text-md rounded bg-gray-100 hover:bg-gray-300"
                                    onClick={() => {
                                      this.onDeleteRow(elm.id);
                                    }}
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      ) : (
                        <tbody>
                          <tr>
                            <td
                              colSpan="4"
                              className="text-center py-3 text-sm"
                            >
                              No Effort Added
                            </td>
                          </tr>
                        </tbody>
                      )}
                    </table>
                  </div>
                </div>
              </div>
              {effort.length > 0 ? (
                <div className="flex flex-col mt-3">
                  <div className="lg:w-12/12 xl:w-12/12 px-2">
                    <div className="flex flex-wrap justify-center py-2">
                      <button
                        className="default-btn"
                        type="button"
                        onClick={() => {
                          this.updateSettings(projectid);
                        }}
                      >
                        Save
                      </button>
                      <Link to="/dashboard">
                        <button
                          className="default-btn-cancel"
                          onClick={this.handleClose}
                          type="button"
                        >
                          Cancel
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              ) : null}
            </form>
          </div>
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default Effort;
