import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { SERVER_URL } from "../../../constants.js";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";
const token = sessionStorage.getItem("jwt");

class ALMSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTab: 1,
      tabNames: [
        {
          name: "Defects",
          id: 1,
        },
        {
          name: "ALM API Details",
          id: 2,
        },
        {
          name: "ALM In Project Details",
          id: 3,
        },
        {
          name: "ALM SIT Details",
          id: 4,
        },
        {
          name: "ALM UAT Details",
          id: 5,
        },
        {
          name: "ALM Module Details",
          id: 6,
        },
        {
          name: "Migration Module Details",
          id: 7,
        },
      ],
      projectid: "",
      id: 0,
      host: "",
      almUsername: "",
      almPassword: "",
      domain: "",
      almProject: "",
      requirementsProjectFolders: {
        projectFieldName: "",
        projectName: [],
      },
      defects: {
        projectFieldName: "",
        projectName: [],
        projectLevelDefects: {
          name: "",
          sit: [],
          uat: [],
          production: [],
          automation: [],
          manual: [],
        },
        moduleLevelDefects: [
          {
            id: 1,
            projectFieldName: "",
            projectName: [""],
            moduleName: "",
            defectModuleFieldName: "",
            tags: [""],
          },
        ],
      },
      almApiDetailsPojo: [],
      almInProjectDetailsPojo: [],
      almSitDetailsPojo: [],
      almUatDetailsPojo: [],
      almModuleDetailsPojo: [],
      migrationModuleDetailsPojo: [],
    };
  }

  componentDidMount() {
    this.fetchProject();
  }

  updateSettings = (projectId) => {
    let sendval = {
      host: this.state.host,
      almUsername: this.state.almUsername,
      almPassword: this.state.almPassword,
      domain: this.state.domain,
      almProject: this.state.almProject,
      defects: this.state.defects,
      almApiDetailsPojo: this.state.almApiDetailsPojo,
      almInProjectDetailsPojo: this.state.almInProjectDetailsPojo,
      almSitDetailsPojo: this.state.almSitDetailsPojo,
      almUatDetailsPojo: this.state.almUatDetailsPojo,
      almModuleDetailsPojo: this.state.almModuleDetailsPojo,
      migrationModuleDetailsPojo: this.state.migrationModuleDetailsPojo,
    };

    fetch(SERVER_URL + "projects/" + projectId + "/almsettings", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(sendval),
    })
      .then((res) => {
        console.log("res :>> ", res);
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };
  fetchProject = () => {
    this.setState({
      projectid: sessionStorage.getItem("projectID"),
    });
    this.getAllalmsettings(sessionStorage.getItem("projectID"));
  };

  getAllalmsettings = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/almsettings", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let almvalues = responseData;
        this.setState({
          host: almvalues.host,
          almUsername: almvalues.almUsername,
          almPassword: almvalues.almPassword,
          domain: almvalues.domain,
          almProject: almvalues.almProject,
          defects: almvalues.defects,
          almApiDetailsPojo: almvalues.almApiDetailsPojo,
          almInProjectDetailsPojo: almvalues.almInProjectDetailsPojo,
          almSitDetailsPojo: almvalues.almSitDetailsPojo,
          almUatDetailsPojo: almvalues.almUatDetailsPojo,
          almModuleDetailsPojo: almvalues.almModuleDetailsPojo,
          migrationModuleDetailsPojo: almvalues.migrationModuleDetailsPojo,
        });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  addMigrationModule = () => {
    this.setState({
      migrationModuleDetailsPojo: this.state.migrationModuleDetailsPojo.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        module: "Module",
        testExecutionFolderId: [],
        testDesignFolderId: [],
      }),
    });
  };
  addAlmSIT = () => {
    this.setState({
      almSitDetailsPojo: this.state.almSitDetailsPojo.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        testExecutionFolderId: [],
        testDesignFolderId: [],
      }),
    });
  };
  addAlmUAT = () => {
    this.setState({
      almUatDetailsPojo: this.state.almUatDetailsPojo.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        testExecutionFolderId: [],
        testDesignFolderId: [],
      }),
    });
  };
  addAlmApiDetails = () => {
    this.setState({
      almApiDetailsPojo: this.state.almApiDetailsPojo.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        testExecutionFolderId: [],
        testDesignFolderId: [],
      }),
    });
  };
  addAlmInProjectDetails = () => {
    this.setState({
      almInProjectDetailsPojo: this.state.almInProjectDetailsPojo.concat({
        id: Math.floor(Math.random() * 90000) + 10000,
        testExecutionFolderId: [],
        testDesignFolderId: [],
      }),
    });
  };
  onDeleteRow = (id) => {
    const items = this.state.migrationModuleDetailsPojo.filter(
      (item) => item.id !== id
    );
    this.setState({ migrationModuleDetailsPojo: items });
  };
  onDeleteAlmSIT = (id) => {
    const items = this.state.almSitDetailsPojo.filter((item) => item.id !== id);
    this.setState({ almSitDetailsPojo: items });
  };
  onDeleteAlmUAT = (id) => {
    const items = this.state.almUatDetailsPojo.filter((item) => item.id !== id);
    this.setState({ almUatDetailsPojo: items });
  };
  onDeleteAlmApiDetails = (id) => {
    const items = this.state.almApiDetailsPojo.filter((item) => item.id !== id);
    this.setState({ almApiDetailsPojo: items });
  };
  onDeleteAlmInProjectDetails = (id) => {
    const items = this.state.almInProjectDetailsPojo.filter(
      (item) => item.id !== id
    );
    this.setState({ almInProjectDetailsPojo: items });
  };
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleChangeModules = (modules) => {
    this.setState({ modules });
  };
  handleChangeRequirements = (requirementsProjectFolders) => {
    this.setState({ requirementsProjectFolders });
  };
  handleChangeDefectsprojectFieldName = (event) => {
    let eventval = event.target.value;
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectFieldName: eventval,
      },
    }));
  };
  handleChangeDefectsprojectName = (name) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectName: name,
      },
    }));
  };
  handleChangeDefectsProjectLevelName = (event) => {
    let eventval = event.target.value;
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          name: eventval,
        },
      },
    }));
  };
  handleChangeDefectsProjectLevelSIT = (sit) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          sit: sit,
        },
      },
    }));
  };
  handleChangeDefectsProjectLevelUAT = (uat) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          uat: uat,
        },
      },
    }));
  };
  handleChangeDefectsProjectLevelproduction = (production) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          production: production,
        },
      },
    }));
  };
  handleChangeDefectsProjectLevelautomation = (automation) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          automation: automation,
        },
      },
    }));
  };
  handleChangeDefectsProjectLevelmanual = (manual) => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        projectLevelDefects: {
          ...prevState.defects.projectLevelDefects,
          manual: manual,
        },
      },
    }));
  };

  handleChangeModuleNameChange = (e, id) => {
    const item = [...this.state.almModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.module = e.target.value;
      }
    });
    this.setState({ almModuleDetailsPojo: item });
  };
  handleChangetestDesignFolderId = (tags, changed, id) => {
    const item = [...this.state.almModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ almModuleDetailsPojo: item });
  };
  handleChangetestExecutionFolderId = (tags, changed, id) => {
    const item = [...this.state.almModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ almModuleDetailsPojo: item });
  };
  handleChangeModuleNameMigration = (e, id) => {
    const item = [...this.state.migrationModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.module = e.target.value;
      }
    });
    this.setState({ migrationModuleDetailsPojo: item });
  };
  handleChangetestDesignFolderIdMigration = (tags, changed, id) => {
    const item = [...this.state.migrationModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ migrationModuleDetailsPojo: item });
  };
  handleChangetestExecutionFolderIdMigration = (tags, changed, id) => {
    const item = [...this.state.migrationModuleDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ migrationModuleDetailsPojo: item });
  };
  handleChangetestDesignFolderIdAlmSIT = (tags, changed, id) => {
    const item = [...this.state.almSitDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ almSitDetailsPojo: item });
  };
  handleChangetestExecutionFolderIdAlmSIT = (tags, changed, id) => {
    const item = [...this.state.almSitDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ almSitDetailsPojo: item });
  };
  handleChangetestDesignFolderIdAlmUAT = (tags, changed, id) => {
    const item = [...this.state.almUatDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ almUatDetailsPojo: item });
  };
  handleChangetestExecutionFolderIdAlmUAT = (tags, changed, id) => {
    const item = [...this.state.almUatDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ almUatDetailsPojo: item });
  };
  handleChangetestDesignFolderApiDetails = (tags, changed, id) => {
    const item = [...this.state.almApiDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ almApiDetailsPojo: item });
  };
  handleChangetestExecutionFolderIdApiDetails = (tags, changed, id) => {
    const item = [...this.state.almApiDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ almApiDetailsPojo: item });
  };
  handleChangetestDesignFolderIdInProjectDetails = (tags, changed, id) => {
    const item = [...this.state.almInProjectDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testDesignFolderId = tags;
      }
    });
    this.setState({ almInProjectDetailsPojo: item });
  };
  handleChangetestExecutionFolderIdInProjectDetails = (tags, changed, id) => {
    const item = [...this.state.almInProjectDetailsPojo];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.testExecutionFolderId = tags;
      }
    });
    this.setState({ almInProjectDetailsPojo: item });
  };
  handleChangemigrationModule = (migrationModule) => {
    this.setState({ migrationModule });
  };
  handleChangemigrationtestDesignFolderId = (migrationtestDesignFolderId) => {
    this.setState({ migrationtestDesignFolderId });
  };

  //Defects Module Level

  handleChangeDefectsModuleName = (e, id) => {
    const item = [...this.state.defects.moduleLevelDefects];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.moduleName = e.target.value;
      }
    });
    console.log(`item`, item);
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: item,
      },
    }));
  };

  handleChangedefectModuleFieldName = (e, id) => {
    const item = [...this.state.defects.moduleLevelDefects];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.defectModuleFieldName = e.target.value;
      }
    });
    console.log(`item`, item);
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: item,
      },
    }));
  };
  handleChangedefectprojectFieldName = (e, id) => {
    const item = [...this.state.defects.moduleLevelDefects];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.projectFieldName = e.target.value;
      }
    });
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: item,
      },
    }));
  };
  handleChangemoduleLevelDefectsprojectName = (tags, changed, id) => {
    const item = [...this.state.defects.moduleLevelDefects];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.projectName = tags;
      }
    });
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: item,
      },
    }));
  };
  handleChangemoduleLevelDefectstags = (tags, changed, id) => {
    const item = [...this.state.defects.moduleLevelDefects];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.tags = tags;
      }
    });
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: item,
      },
    }));
  };
  onDeletemoduleLevelDefects = (id) => {
    const items = this.state.defects.moduleLevelDefects.filter(
      (item) => item.id !== id
    );
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: items,
      },
    }));
  };

  addmoduleLevelDefects = () => {
    this.setState((prevState) => ({
      defects: {
        ...prevState.defects,
        moduleLevelDefects: this.state.defects.moduleLevelDefects.concat({
          id: Math.floor(Math.random() * 90000) + 10000,
          projectFieldName: "",
          projectName: [],
          moduleName: "",
          defectModuleFieldName: "",
          tags: [],
        }),
      },
    }));
  };

  // handleChangeDefectsProjectLevelautomation = (automation) => {
  //   this.setState((prevState) => ({
  //     defects: {
  //       ...prevState.defects,
  //       projectLevelDefects: {
  //         ...prevState.defects.projectLevelDefects,
  //         automation: automation,
  //       },
  //     },
  //   }));
  // };

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const {
      newTab,
      tabNames,
      projectid,
      host,
      almUsername,
      almPassword,
      domain,
      almProject,
      almModuleDetailsPojo,
      migrationModuleDetailsPojo,
      almSitDetailsPojo,
      almUatDetailsPojo,
      almApiDetailsPojo,
      almInProjectDetailsPojo,
      defects,
    } = this.state;
    return (
      <>
        <div className=" mb-0 py-4">
          <div className="flex flex-row">
            <div className="w-full xl:w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">ALM Settings</h6>
            </div>
            <div className="w-full xl:w-6/12 text-right">
              <button
                onClick={this.handleClose}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-times mr-1 text-md text-white"></i> Close
              </button>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className="w-full">
            <div className="flex flex-wrap bg-white p-4 px-1 rounded-lg shadow-lg">
              <div className="w-1/5 px-4">
                <div className="relative w-full mb-3">
                  <label className="metrics-label">Host</label>
                  <input
                    type="text"
                    name="host"
                    className="metrics-input"
                    placeholder="https://"
                    defaultValue={host}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="lg:w-1/5 px-4">
                <div className="relative w-full mb-3">
                  <label className="metrics-label">Username</label>
                  <input
                    type="text"
                    name="almUsername"
                    className="metrics-input"
                    defaultValue={almUsername}
                    onChange={this.handleChange}
                    autoComplete="off"
                  />
                </div>
              </div>
              <div className="lg:w-1/5 px-4">
                <div className="relative w-full mb-3">
                  <label className="metrics-label">Password</label>
                  <input
                    type="password"
                    name="almPassword"
                    className="metrics-input"
                    defaultValue={almPassword}
                    onChange={this.handleChange}
                    autoComplete="off"
                  />
                </div>
              </div>
              <div className="w-1/5 px-4">
                <div className="relative w-full mb-3">
                  <label className="metrics-label">Domain</label>
                  <input
                    type="text"
                    name="domain"
                    className="metrics-input"
                    defaultValue={domain}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="w-1/5 px-4">
                <div className="relative w-full mb-3">
                  <label className="metrics-label">Project</label>
                  <input
                    type="text"
                    name="almProject"
                    className="metrics-input"
                    defaultValue={almProject}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className="w-full">
            <div className="flex py-3">
              <div className="flex px-4 py-3 bg-white rounded-lg shadow-lg">
                <ul
                  className="flex-grow w-64 relative mb-0 list-none pb-4 flex-row "
                  role="tablist"
                >
                  {tabNames.map((tab) => {
                    return (
                      <li
                        className="my-1 w-100 flex-auto text-left"
                        key={tab.id}
                      >
                        <a
                          className={
                            "text-xs font-bold capitalize px-4 py-2 block leading-normal " +
                            (newTab === tab.id
                              ? "text-white bg-blue-800 "
                              : "text-gray-900 bg-blue-100 hover:bg-blue-800 hover:text-white")
                          }
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              newTab: tab.id,
                            });
                          }}
                          data-toggle="tab"
                          href={`#tab${tab.id}`}
                          role="tablist"
                          key={tab.id}
                        >
                          {tab.name}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className="relative  min-w-0 break-words w-full px-4 py-3  ml-3 bg-white rounded-lg shadow-lg">
                <div className="">
                  <div className="tab-content tab-space">
                    <div
                      className={newTab === 1 ? "block" : "hidden"}
                      id={`tab1`}
                      key={1}
                    >
                      <div className="flex flex-wrap">
                        <div className="w-4/12 px-1">
                          <div className="relative w-full mb-3">
                            <label className="metrics-label">
                              Project Field Name
                            </label>
                            <input
                              type="text"
                              name="projectFieldName"
                              className="metrics-input"
                              placeholder=""
                              defaultValue={defects.projectFieldName}
                              onChange={
                                this.handleChangeDefectsprojectFieldName
                              }
                            />
                          </div>
                        </div>
                        <div className="w-4/12 px-4">
                          <div className="relative w-full mb-3">
                            <label className="metrics-label">
                              Defects Project Name
                            </label>
                            <TagsInput
                              name=""
                              className="tagsinput"
                              placeholder="Add a Project Name"
                              value={defects.projectName}
                              onChange={this.handleChangeDefectsprojectName}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="bg-blueGray-100 mx-1">
                        <p className="bg-blue-800 text-white px-2 py-1 text-xs mb-1">
                          Project Level Defects
                        </p>
                        <div className="flex flex-wrap">
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Defect Field Name
                              </label>
                              <input
                                type="text"
                                name="name"
                                className="metrics-input"
                                placeholder=""
                                defaultValue={defects.projectLevelDefects.name}
                                onChange={
                                  this.handleChangeDefectsProjectLevelName
                                }
                              />
                            </div>
                          </div>
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Project Level Defect - SIT
                              </label>
                              <TagsInput
                                name="projectLevelDefectssit"
                                className="tagsinput"
                                placeholder="Add a Project Name"
                                value={defects.projectLevelDefects.sit}
                                onChange={
                                  this.handleChangeDefectsProjectLevelSIT
                                }
                              />
                            </div>
                          </div>
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Project Level Defect - UAT
                              </label>
                              <TagsInput
                                name="projectLevelDefectsuat"
                                className="tagsinput"
                                placeholder="Add a Project Name"
                                value={defects.projectLevelDefects.uat}
                                onChange={
                                  this.handleChangeDefectsProjectLevelUAT
                                }
                              />
                            </div>
                          </div>
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Project Level Defect - Production
                              </label>
                              <TagsInput
                                name="projectLevelDefectsproduction"
                                className="tagsinput"
                                placeholder="Add a Project Name"
                                value={defects.projectLevelDefects.production}
                                onChange={
                                  this.handleChangeDefectsProjectLevelproduction
                                }
                              />
                            </div>
                          </div>
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Project Level Defect - Automation
                              </label>
                              <TagsInput
                                name="projectLevelDefectsautomation"
                                className="tagsinput"
                                placeholder="Add a Project Name"
                                value={defects.projectLevelDefects.automation}
                                onChange={
                                  this.handleChangeDefectsProjectLevelautomation
                                }
                              />
                            </div>
                          </div>
                          <div className="w-4/12 px-4">
                            <div className="relative w-full mb-3">
                              <label className="metrics-label">
                                Project Level Defect - Manual
                              </label>
                              <TagsInput
                                name="projectLevelDefectsmanual"
                                className="tagsinput"
                                placeholder="Add a Project Name"
                                value={defects.projectLevelDefects.manual}
                                onChange={
                                  this.handleChangeDefectsProjectLevelmanual
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="bg-blueGray-100 mx-1 mt-3">
                        <p className="bg-blue-800 text-white px-2 py-1 text-xs mb-1">
                          Module Level Defects
                        </p>
                        <div className="flex flex-wrap">
                          <div className="w-full">
                            <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                              <table className="moduletable min-w-full leading-normal border border-gray-100">
                                <thead>
                                  <tr>
                                    <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                      Module Name
                                    </th>
                                    <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                      Defect Module Field Name
                                    </th>
                                    <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                      Project Field Name
                                    </th>
                                    <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                      Project Name
                                    </th>
                                    <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                      Tags
                                    </th>
                                    <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                                  </tr>
                                </thead>

                                <tbody>
                                  {defects.moduleLevelDefects.map((elm) => {
                                    return (
                                      <tr key={elm.id}>
                                        <td>
                                          <input
                                            type="text"
                                            name="moduleName"
                                            className="metrics-input-addmodules"
                                            value={elm.moduleName}
                                            onChange={(e) =>
                                              this.handleChangeDefectsModuleName(
                                                e,
                                                elm.id
                                              )
                                            }
                                          />
                                        </td>
                                        <td>
                                          <input
                                            type="text"
                                            name="defectModuleFieldName"
                                            className="metrics-input-addmodules"
                                            value={elm.defectModuleFieldName}
                                            onChange={(e) =>
                                              this.handleChangedefectModuleFieldName(
                                                e,
                                                elm.id
                                              )
                                            }
                                          />
                                        </td>
                                        <td>
                                          <input
                                            type="text"
                                            name="projectFieldName"
                                            className="metrics-input-addmodules"
                                            value={elm.projectFieldName}
                                            onChange={(e) =>
                                              this.handleChangedefectprojectFieldName(
                                                e,
                                                elm.id
                                              )
                                            }
                                          />
                                        </td>
                                        <td>
                                          <TagsInput
                                            name="testDesignFolderId"
                                            className="tagsinput"
                                            placeholder="Add a Module"
                                            value={elm.projectName}
                                            onChange={(tags, changed) =>
                                              this.handleChangemoduleLevelDefectsprojectName(
                                                tags,
                                                changed,
                                                elm.id
                                              )
                                            }
                                          />
                                        </td>
                                        <td>
                                          <TagsInput
                                            name="testExecutionFolderId"
                                            className="tagsinput"
                                            placeholder="Add a Module"
                                            value={elm.tags}
                                            onChange={(tags, changed) =>
                                              this.handleChangemoduleLevelDefectstags(
                                                tags,
                                                changed,
                                                elm.id
                                              )
                                            }
                                          />
                                        </td>
                                        <td className="text-center cursor-pointer">
                                          <i
                                            className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                            onClick={() => {
                                              this.onDeletemoduleLevelDefects(
                                                elm.id
                                              );
                                            }}
                                          ></i>
                                        </td>
                                      </tr>
                                    );
                                  })}
                                </tbody>
                              </table>
                              <div className="flex flex-col">
                                <div className="lg:w-12/12 xl:w-12/12">
                                  <div className="flex flex-wrap justify-center py-2">
                                    <button
                                      className="default-btn-sm float-right"
                                      type="button"
                                      onClick={() => {
                                        this.addmoduleLevelDefects();
                                      }}
                                    >
                                      <i className="fas fa-plus text-md text-white"></i>{" "}
                                      Add Module Level Defects
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 2 ? "block" : "hidden"}
                      id={`tab2`}
                      key={2}
                    >
                      <div className="w-full">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="w-40 px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  ALM API Details
                                </th>
                              </tr>
                              <tr>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Execution ID's
                                </th>
                                <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                              </tr>
                            </thead>

                            <tbody>
                              {almApiDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderApiDetails(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderIdApiDetails(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td className="text-center cursor-pointer">
                                      <i
                                        className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                        onClick={() => {
                                          this.onDeleteAlmApiDetails(elm.id);
                                        }}
                                      ></i>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <div className="flex flex-col">
                            <div className="lg:w-12/12 xl:w-12/12">
                              <div className="flex flex-wrap justify-center py-2">
                                <button
                                  className="default-btn-sm float-right"
                                  type="button"
                                  onClick={() => {
                                    this.addAlmApiDetails();
                                  }}
                                >
                                  <i className="fas fa-plus text-md text-white"></i>{" "}
                                  Add Modules
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 3 ? "block" : "hidden"}
                      id={`tab3`}
                      key={3}
                    >
                      <div className="w-full ">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  ALM In Project Details
                                </th>
                              </tr>
                              <tr>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Execution ID's
                                </th>
                                <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                              </tr>
                            </thead>

                            <tbody>
                              {almInProjectDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderIdInProjectDetails(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderIdInProjectDetails(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td className="text-center cursor-pointer">
                                      <i
                                        className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                        onClick={() => {
                                          this.onDeleteAlmInProjectDetails(
                                            elm.id
                                          );
                                        }}
                                      ></i>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <div className="flex flex-col">
                            <div className="lg:w-12/12 xl:w-12/12">
                              <div className="flex flex-wrap justify-center py-2">
                                <button
                                  className="default-btn-sm float-right"
                                  type="button"
                                  onClick={() => {
                                    this.addAlmInProjectDetails();
                                  }}
                                >
                                  <i className="fas fa-plus text-md text-white"></i>{" "}
                                  Add Modules
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 4 ? "block" : "hidden"}
                      id={`tab4`}
                      key={4}
                    >
                      <div className="w-full ">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="w-40 px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  ALM SIT Details
                                </th>
                              </tr>
                              <tr>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Execution ID's
                                </th>
                                <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                              </tr>
                            </thead>

                            <tbody>
                              {almSitDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderIdAlmSIT(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderIdAlmSIT(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td className="text-center cursor-pointer">
                                      <i
                                        className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                        onClick={() => {
                                          this.onDeleteAlmSIT(elm.id);
                                        }}
                                      ></i>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <div className="flex flex-col">
                            <div className="lg:w-12/12 xl:w-12/12">
                              <div className="flex flex-wrap justify-center py-2">
                                <button
                                  className="default-btn-sm float-right"
                                  type="button"
                                  onClick={() => {
                                    this.addAlmSIT();
                                  }}
                                >
                                  <i className="fas fa-plus text-md text-white"></i>{" "}
                                  Add Modules
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 5 ? "block" : "hidden"}
                      id={`tab5`}
                      key={5}
                    >
                      <div className="w-full ">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="w-40 px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  ALM UAT Details
                                </th>
                              </tr>
                              <tr>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Test Execution ID's
                                </th>
                                <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                              </tr>
                            </thead>

                            <tbody>
                              {almUatDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderIdAlmUAT(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderIdAlmUAT(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td className="text-center cursor-pointer">
                                      <i
                                        className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                        onClick={() => {
                                          this.onDeleteAlmUAT(elm.id);
                                        }}
                                      ></i>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <div className="flex flex-col">
                            <div className="lg:w-12/12 xl:w-12/12">
                              <div className="flex flex-wrap justify-center py-2">
                                <button
                                  className="default-btn-sm float-right"
                                  type="button"
                                  onClick={() => {
                                    this.addAlmUAT();
                                  }}
                                >
                                  <i className="fas fa-plus text-md text-white"></i>{" "}
                                  Add Modules
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 6 ? "block" : "hidden"}
                      id={`tab6`}
                      key={6}
                    >
                      <div className="w-full ">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="w-40 px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  ALM Modules Details
                                </th>
                              </tr>
                              <tr>
                                <th className="w-56 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Module Name
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Execution ID's
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {almModuleDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <input
                                        type="text"
                                        name="module"
                                        className="metrics-input-addmodules"
                                        defaultValue={elm.module}
                                        onChange={(e) =>
                                          this.handleChangeModuleNameChange(
                                            e,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderId(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderId(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div
                      className={newTab === 7 ? "block" : "hidden"}
                      id={`tab7`}
                      key={7}
                    >
                      <div className="w-full ">
                        <div className="inline-block min-w-full overflow-hidden bg-white rounded-lg">
                          <table className="moduletable min-w-full leading-normal border border-gray-100">
                            <thead>
                              <tr>
                                <th
                                  colSpan="4"
                                  className="w-40 px-4 py-2 bg-blue-800 text-white text-center text-xs captalize font-medium border-b border-t border-blueGray-200"
                                >
                                  Migration Modules
                                </th>
                              </tr>
                              <tr>
                                <th className="w-56 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Module
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Design ID's
                                </th>
                                <th className="px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200">
                                  Execution ID's
                                </th>
                                <th className="w-12 px-4 py-2 bg-metrics-tableth  text-blueGray-800  text-center text-xs captalize font-medium border-b border-t border-blueGray-200"></th>
                              </tr>
                            </thead>

                            <tbody>
                              {migrationModuleDetailsPojo.map((elm) => {
                                return (
                                  <tr key={elm.id}>
                                    <td>
                                      <input
                                        type="text"
                                        name="projectFolderName"
                                        className="metrics-input-addmodules"
                                        value={elm.module}
                                        onChange={(e) =>
                                          this.handleChangeModuleNameMigration(
                                            e,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testDesignFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testDesignFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestDesignFolderIdMigration(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td>
                                      <TagsInput
                                        name="testExecutionFolderId"
                                        className="tagsinput"
                                        placeholder="Add a Module"
                                        value={elm.testExecutionFolderId}
                                        onChange={(tags, changed) =>
                                          this.handleChangetestExecutionFolderIdMigration(
                                            tags,
                                            changed,
                                            elm.id
                                          )
                                        }
                                      />
                                    </td>
                                    <td className="text-center cursor-pointer">
                                      <i
                                        className="fas fa-times mr-1 p-1 px-2 text-md rounded bg-gray-100 hover:bg-gray-300"
                                        onClick={() => {
                                          this.onDeleteRow(elm.id);
                                        }}
                                      ></i>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <div className="flex flex-col">
                            <div className="lg:w-12/12 xl:w-12/12">
                              <div className="flex flex-wrap justify-center py-2">
                                <button
                                  className="default-btn-sm float-right"
                                  type="button"
                                  onClick={() => {
                                    this.addMigrationModule();
                                  }}
                                >
                                  <i className="fas fa-plus text-md text-white"></i>{" "}
                                  Add Modules
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col mt-3">
              <div className="lg:w-12/12 xl:w-12/12 px-2">
                <div className="flex flex-wrap justify-center py-2">
                  <button
                    className="default-btn"
                    type="button"
                    onClick={() => {
                      this.updateSettings(projectid);
                    }}
                  >
                    Save
                  </button>
                  <Link to="/dashboard">
                    <button
                      className="default-btn-cancel"
                      onClick={this.handleClose}
                      type="button"
                    >
                      Cancel
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default ALMSettings;
