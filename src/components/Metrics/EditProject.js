import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import { Controller, useForm } from "react-hook-form";
import { SERVER_URL } from "../../constants";
const token = sessionStorage.getItem("jwt");

const EditProject = (props) => {
  const [open, setOpen] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const [project, setProject] = useState([]);

  useEffect(() => {
    setProject({ ...props.project });
    if (props.showModal) {
      setOpen(true);
    }
    return () => {};
  }, [props.project, props.showModal]);

  const updatevalues = (e) => {
    const item = { ...project };
    Object.keys(item).forEach(function (key) {
      if (e.currentTarget.name === key) {
        item[key] = e.currentTarget.value;
      }
    });
    setProject({ ...item });
  };
  const updateDatevalues = (date, keyname, e) => {
    console.log(
      `moment(startDate).format("DD-MM-yyyy")`,
      moment(date).format("DD-MM-yyyy")
    );
    const d = moment(date).format("DD-MM-yyyy");
    const item = { ...project };
    Object.keys(item).forEach(function (key) {
      if (keyname === key) {
        item[key] = d;
      }
    });
    setProject({ ...item });
  };

  const onSubmit = (data) => {
    data.startDate = moment(startDate).format("DD-MM-yyyy");
    data.endDate = moment(endDate).format("DD-MM-yyyy");

    console.log("The modules response from the server is : " + data);
    props.updateProject(project, props.project.id);
    handleClose();
  };
  const handleClose = () => {
    setOpen(false);
    props.changeModal();
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">Edit Project</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-4">
            {project === null ? null : (
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="flex flex-wrap">
                  <div className="w-full lg:w-12/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Name</label>
                      <input
                        type="text"
                        className="metrics-input"
                        value={project.name}
                        name="name"
                        {...register("name", {
                          required: "Please enter a project name",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      />
                      {errors.name && (
                        <div className="text-sm text-red-500 relative">
                          {errors.name.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Project Type</label>
                      <select
                        type="text"
                        className="metrics-input"
                        value={project.type}
                        name="type"
                        {...register("type", {
                          required: "Please enter a type",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      >
                        <option value="Fix Bid Transformation">
                          Fix Bid Transformation
                        </option>
                        <option value="MTM Transformation">
                          MTM Transformation
                        </option>
                        <option value="MTM BAU">MTM BAU</option>
                        <option value="T&M Staffing Transformation">
                          T&M Staffing Transformation
                        </option>
                        <option value="T&M Staffing BAU">
                          T&M Staffing BAU
                        </option>
                      </select>
                      {errors.type && (
                        <div className="text-sm text-red-500 relative">
                          {errors.type.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Tribe</label>
                      <select
                        type="text"
                        className="metrics-input"
                        value={project.cluster}
                        name="cluster"
                        {...register("cluster", {
                          required: "Please select a tribe",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      >
                        <option value="KRAU Bench">KRAU Bench</option>
                        <option value="Tribe-1">Tribe-1</option>
                        <option value="Tribe-2">Tribe-2</option>
                        <option value="Tribe-3">Tribe-3</option>
                        <option value="Tribe-4">Tribe-4</option>
                        <option value="Tribe-5">Tribe-5</option>
                        <option value="Tribe-6">Tribe-6</option>
                        <option value="Tribe-7">Tribe-7</option>
                        <option value="Tribe-8">Tribe-8</option>
                      </select>

                      {errors.cluster && (
                        <div className="text-sm text-red-500 relative">
                          {errors.cluster.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Domain</label>
                      <input
                        type="text"
                        className="metrics-input"
                        value={project.domain}
                        name="domain"
                        {...register("domain", {
                          required: "Please enter a domain",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      />
                      {errors.domain && (
                        <div className="text-sm text-red-500 relative">
                          {errors.domain.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Delivery Manager</label>
                      <input
                        type="text"
                        className="metrics-input"
                        value={project.deliveryManager}
                        name="deliveryManager"
                        {...register("deliveryManager", {
                          required: "Please enter a deliveryManager",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      />
                      {errors.deliveryManager && (
                        <div className="text-sm text-red-500 relative">
                          {errors.deliveryManager.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Technology</label>
                      <input
                        type="text"
                        className="metrics-input"
                        value={project.technology}
                        name="technology"
                        {...register("technology", {
                          required: "Please enter a technology",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      />
                      {errors.technology && (
                        <div className="text-sm text-red-500 relative">
                          {errors.technology.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Project Category</label>
                      <select
                        type="text"
                        className="metrics-input"
                        value={project.category}
                        name="category"
                        {...register("category", {
                          required: "Please select a category",
                        })}
                        onChange={(e) => {
                          updatevalues(e);
                        }}
                      >
                        <option value="Simple">Simple</option>
                        <option value="Medium">Medium</option>
                        <option value="Complex">Complex</option>
                      </select>

                      {errors.category && (
                        <div className="text-sm text-red-500 relative">
                          {errors.category.message}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">Start Date</label>
                      <Controller
                        control={control}
                        name="startDate"
                        value={project.startDate}
                        render={() => (
                          <DatePicker
                            className="metrics-input"
                            dateFormat="dd-MM-yyyy"
                            name="startDate"
                            onChange={(date, e) => {
                              updateDatevalues(date, "startDate", e);
                            }}
                            selected={
                              new Date(
                                moment(project.startDate, "DD-MM-YYYY").format(
                                  "MM-DD-YYYY"
                                )
                              )
                            }
                            popperPlacement="top"
                          />
                        )}
                      />
                    </div>
                  </div>
                  <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                    <div className="relative w-full mb-3">
                      <label className="metrics-label">End Date</label>
                      <Controller
                        control={control}
                        name="endDate"
                        value={project.endDate}
                        render={() => (
                          <DatePicker
                            className="metrics-input"
                            dateFormat="dd-MM-yyyy"
                            selected={
                              new Date(
                                moment(project.endDate, "DD-MM-YYYY").format(
                                  "MM-DD-YYYY"
                                )
                              )
                            }
                            onChange={(date, e) => {
                              updateDatevalues(date, "endDate", e);
                            }}
                            popperPlacement="top"
                          />
                        )}
                      />
                    </div>
                  </div>
                </div>

                <div className="flex flex-wrap justify-center py-3">
                  <button className="default-btn" type="submit">
                    Save
                  </button>
                  <button
                    className="default-btn-cancel"
                    type="button"
                    onClick={handleClose}
                  >
                    Cancel
                  </button>
                </div>
              </form>
            )}
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default EditProject;
