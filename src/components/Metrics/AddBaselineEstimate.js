import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const AddBaselineEstimate = (props) => {
  const [open, setOpen] = useState(false);
  const [project, setProject] = useState({id: '', name: '', type: '', cluster: '', domain: '', deliveryManager: '', technology: '', category: '', startDate: '', endDate: '', lastUpdateOn: ''});

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setProject({...project, [event.target.name]: event.target.value});
  }

  // Save project and close modal form
  const handleSave = () => {
    props.addProject(project);
    handleClose();
  }

  return (
    <div>
      <button style={{margin: 10}} onClick={handleClickOpen}>New Project</button>
      <Dialog open={open} onClose={handleClose}>
          <DialogTitle>New Project</DialogTitle>
          <DialogContent>
            <label>Name</label>		  
            <input type="text" name="name" 
              value={project.name} onChange={handleChange}/><br/> 
            <label>Type</label>		                
            <input type="text" name="type" 
              value={project.type} onChange={handleChange}/><br/>
            <label>Cluster</label>		                
            <input type="text" name="cluster" 
              value={project.cluster} onChange={handleChange}/><br/>
            <label>Domain</label>		                
            <input type="text" name="domain" 
              value={project.domain} onChange={handleChange}/><br/>
            <label>DeliveryManager</label>		                
            <input type="text" name="deliveryManager" 
              value={project.deliveryManager} onChange={handleChange}/><br/>
            <label>Technology</label>		                
            <input type="text" name="technology" 
              value={project.technology} onChange={handleChange}/><br/>
            <label>Category</label>		                
            <input type="text" name="category" 
              value={project.category} onChange={handleChange}/><br/>
            <label>StartDate</label>		                
            <input type="text" name="startDate" 
              value={project.startDate} onChange={handleChange}/><br/>
            <label>EndDate</label>		                
            <input type="text" name="endDate" 
              value={project.endDate} onChange={handleChange}/><br/>	
            <label>LastUpdateOn</label>		                		  
            <input type="text" name="lastUpdateOn" 
              value={project.lastUpdateOn} onChange={handleChange}/><br/>			  
          </DialogContent>
          <DialogActions>
            <button onClick={handleClose}>Cancel</button>
            <button onClick={handleSave}>Save</button>
          </DialogActions>
        </Dialog>      
    </div>
  );
};

export default AddBaselineEstimate;