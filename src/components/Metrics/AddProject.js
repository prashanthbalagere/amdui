import React, { useState, useRef } from "react";
import Dialog from "@material-ui/core/Dialog";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import { Controller, useForm } from "react-hook-form";
import { SERVER_URL } from "../../constants";
const token = sessionStorage.getItem("jwt");

const AddProject = (props) => {
  const inputRef1 = useRef(null);
  const inputRef2 = useRef(null);
  const [open, setOpen] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const initialState = {
    id: "",
    name: "",
    type: "",
    cluster: "",
    domain: "",
    deliveryManager: "",
    technology: "",
    category: "",
    startDate: "",
    endDate: "",
    lastUpdateOn: "",
  };
  const [project, setProject] = useState(initialState);

  const handleClickOpen = () => {
    setProject(initialState);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = (data) => {
    data.startDate = moment(startDate).format("DD-MM-yyyy");
    data.endDate = moment(endDate).format("DD-MM-yyyy");
    props.addProject(data);
    handleClose();
    reset({});
  };
  return (
    <>
      <button className="default-btn" type="button" onClick={handleClickOpen}>
        <i className="fas fa-plus mr-1 text-md text-white"></i> New Project
      </button>

      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">New Project</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-4">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex flex-wrap">
                <div className="w-full lg:w-12/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Name</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={project.name}
                      name="name"
                      {...register("name", {
                        required: "Please enter a project name",
                      })}
                    />
                    {errors.name && (
                      <div className="text-sm text-red-500 relative">
                        {errors.name.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Project Type</label>
                    <select
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="type"
                      {...register("type", {
                        required: "Please enter a type",
                      })}
                    >
                      <option value="Fix Bid Transformation">
                        Fix Bid Transformation
                      </option>
                      <option value="MTM Transformation">
                        MTM Transformation
                      </option>
                      <option value="MTM BAU">MTM BAU</option>
                      <option value="T&M Staffing Transformation">
                        T&M Staffing Transformation
                      </option>
                      <option value="T&M Staffing BAU">T&M Staffing BAU</option>
                    </select>
                    {errors.type && (
                      <div className="text-sm text-red-500 relative">
                        {errors.type.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Tribe</label>
                    <select
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="cluster"
                      {...register("cluster", {
                        required: "Please select a tribe",
                      })}
                    >
                      <option value="KRAU Bench">KRAU Bench</option>
                      <option value="Tribe-1">Tribe-1</option>
                      <option value="Tribe-2">Tribe-2</option>
                      <option value="Tribe-3">Tribe-3</option>
                      <option value="Tribe-4">Tribe-4</option>
                      <option value="Tribe-5">Tribe-5</option>
                      <option value="Tribe-6">Tribe-6</option>
                      <option value="Tribe-7">Tribe-7</option>
                      <option value="Tribe-8">Tribe-8</option>
                    </select>
                    {/* <input
                      type="text"
                      className="metrics-input"
                      defaultValue={project.cluster}
                      name="cluster"
                      {...register("cluster", {
                        required: "Please select a tribe",
                      })}
                    /> */}
                    {errors.cluster && (
                      <div className="text-sm text-red-500 relative">
                        {errors.cluster.message}
                      </div>
                    )}
                  </div>
                </div>

                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Domain</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="domain"
                      {...register("domain", {
                        required: "Please enter a domain",
                      })}
                    />
                    {errors.domain && (
                      <div className="text-sm text-red-500 relative">
                        {errors.domain.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Delivery Manager</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="deliveryManager"
                      {...register("deliveryManager", {
                        required: "Please enter a deliveryManager",
                      })}
                    />
                    {errors.deliveryManager && (
                      <div className="text-sm text-red-500 relative">
                        {errors.deliveryManager.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Technology</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="technology"
                      {...register("technology", {
                        required: "Please enter a technology",
                      })}
                    />
                    {errors.technology && (
                      <div className="text-sm text-red-500 relative">
                        {errors.technology.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Project Category</label>
                    <select
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="category"
                      {...register("category", {
                        required: "Please select a category",
                      })}
                    >
                      <option value="Simple">Simple</option>
                      <option value="Medium">Medium</option>
                      <option value="Complex">Complex</option>
                    </select>
                    {/* <input
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="category"
                      {...register("category", {
                        required: "Please enter a category",
                      })}
                    /> */}
                    {errors.category && (
                      <div className="text-sm text-red-500 relative">
                        {errors.category.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Start Date</label>
                    <Controller
                      control={control}
                      name="startDate"
                      defaultValue={startDate}
                      render={() => (
                        <DatePicker
                          className="metrics-input"
                          dateFormat="dd-MM-yyyy"
                          name="startDate"
                          onChange={(startDate) => setStartDate(startDate)}
                          selected={startDate}
                          popperPlacement="top"
                        />
                      )}
                     
                    />
                  
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">End Date</label>
                    <Controller
                      control={control}
                      name="endDate"
                      defaultValue={endDate}
                      render={() => (
                        <DatePicker
                          className="metrics-input"
                          dateFormat="dd-MM-yyyy"
                          onChange={(endDate) => setEndDate(endDate)}
                          selected={endDate}
                          popperPlacement="top"
                        />
                      )}
                     
                    />
                    
                  </div>
                </div>
              </div>
              <div className="flex flex-wrap justify-center py-3">
                <button className="default-btn" type="submit">
                  Save
                </button>
                <button
                  className="default-btn-cancel"
                  type="button"
                  onClick={handleClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default AddProject;
