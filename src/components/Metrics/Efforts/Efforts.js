import React, { Component } from "react";
import { SERVER_URL } from "../../../constants";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import Flatpickr from "react-flatpickr";
import AddEffort from "./AddEffort";
const token = sessionStorage.getItem("jwt");

class Efforts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectid: "",
      ddates: {},
      effort: [],
      types: [],
      hide: false,
    };
  }

  componentDidMount() {
    this.fetchAllEfforts();
  }

  fetchAllEfforts = () => {
    this.setState({
      projectid: sessionStorage.getItem("projectID"),
    });

    this.getEfforts(sessionStorage.getItem("projectID"));
  };

  getEfforts = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/getEffortDetails", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.efforts;
        this.setState({
          effort: datares,
        });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };

  // Add new module
  // addModule(currentProjectId, module) {
  //   fetch(SERVER_URL + "projects/" + currentProjectId + "/addModule", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //       Authorization: token,
  //     },
  //     body: JSON.stringify(module),
  //   })
  //     .then((res) => {
  //       if (res.status === 200) {
  //         this.fetchAllEfforts();
  //         toast.success("Module added successfully", {
  //           position: toast.POSITION.BOTTOM_LEFT,
  //         });
  //       } else {
  //         toast.error("Error when saving", {
  //           position: toast.POSITION.BOTTOM_LEFT,
  //         });
  //       }
  //     })
  //     .catch((err) => {
  //       toast.error("Error when saving", {
  //         position: toast.POSITION.BOTTOM_LEFT,
  //       });
  //     });
  // }

  // Update module
  // updateModule = (module, link, projectId) => {
  //   fetch(SERVER_URL + "projects/" + projectId + "/updateModule/" + link, {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //       Authorization: token,
  //     },
  //     body: JSON.stringify(module),
  //   })
  //     .then((res) => {
  //       if (res.status === 200) {
  //         toast.success("Changes saved successfully", {
  //           position: toast.POSITION.BOTTOM_LEFT,
  //         });
  //         this.getEfforts(this.state.projectid);
  //       }
  //     })
  //     .catch((err) =>
  //       toast.error("Error when saving", {
  //         position: toast.POSITION.BOTTOM_LEFT,
  //       })
  //     );
  // };
  // addEffort = () => {
  //   this.setState({
  //     effort: this.state.effort.concat({
  //       id: Math.floor(Math.random() * 90000) + 10000,
  //       name: this.state.types[0].id,
  //       startDate: null,
  //       endDate: null,
  //       numberOfResources: "",
  //     }),
  //   });
  // };
  HandleTypeChange = (e, id) => {
    const item = [...this.state.effort];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.name = e.target.value;
      }
    });
    this.setState({ effort: item });
  };
  handleChangeResourceChange = (e, id) => {
    const item = [...this.state.effort];
    item.forEach((elm) => {
      if (elm.id === id) {
        elm.numberOfResources = e.target.value;
      }
    });
    this.setState({ effort: item });
  };

  handleChangeDate = (date, id) => {
    console.log(`date`, date);
    var data = [...this.state.effort];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].startDate = moment(date[0]).format("DD-MM-YYYY");
    data[index].endDate = moment(date[1]).format("DD-MM-YYYY");
    this.setState({ data });
    this.collectDisabledDates(data);
  };

  findDisabledValues = (t, i) => {
    return i
      .filter((obj) => obj.name.includes(t))
      .map((item) => ({
        from: item.startDate,
        to: item.endDate,
      }));
  };
  collectDisabledDates = (items) => {
    const result = items.reduce(
      (obj, cur) => ({
        ...obj,
        [cur.name]: this.findDisabledValues(cur.name, items),
      }),
      {}
    );
    console.log(`result`, result);
    this.setState({ ddates: result });
  };

  onDeleteRow = (id) => {
    const tobedeleted = this.state.effort.filter((item) => item.id === id);
    console.log(`object`, tobedeleted);
    fetch(SERVER_URL + "projects/" + this.state.projectid + "/deleteEfforts", {
      method: "POST",
      body: JSON.stringify(tobedeleted),
      headers: { "Content-Type": "application/json", Authorization: token },
    })
      .then((res) => {
        if (res.status === 200) {
          const items = this.state.effort.filter((item) => item.id == id);
          this.setState({ effort: items });
          this.fetchAllEfforts();
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const columns = [
      {
        Header: "Type",
        accessor: "name",
        width: 440,
      },
      {
        Header: "Start Date",
        accessor: "startDate",
        width: 100,
        Cell: ({ value, row }) => (
          <div style={{ textAlign: "center" }}>{row.startDate}</div>
        ),
      },
      {
        Header: "End Date",
        accessor: "endDate",
        width: 100,
        Cell: ({ value, row }) => (
          <div style={{ textAlign: "center" }}>{row.endDate}</div>
        ),
      },
      {
        Header: "Number of Resources",
        accessor: "numberOfResources",
        width: 150,
        Cell: ({ value, row }) => (
          <div style={{ textAlign: "right" }}>{row.numberOfResources}</div>
        ),
      },
      {
        sortable: false,
        filterable: false,
        width: 80,
        accessor: "id",
        Cell: ({ value, row }) => (
          <div style={{ textAlign: "right" }}>
            <i
              className="fas fa-times mr-1 p-1 px-2 ml-3 text-md rounded bg-gray-100 hover:bg-gray-300"
              onClick={() => {
                this.onDeleteRow(row.id);
              }}
            ></i>
          </div>
        ),
      },
    ];

    return (
      <>
        <div className="flex  justify-center mb-0 py-4">
          <div className="flex flex-row w-8/12">
            <div className="w-full xl:w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">Effort</h6>
            </div>
            <div className="w-full xl:w-6/12 text-right">
              <AddEffort fetchAllEfforts={this.fetchAllEfforts} />
              <button
                onClick={this.handleClose}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-times mr-1 text-md text-white"></i> Close
              </button>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className=" bg-white p-4 px-5 rounded-lg shadow-lg w-8/12">
            <ReactTable
              data={this.state.effort}
              columns={columns}
              defaultPageSize={10}
            />
          </div>
        </div>

        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default Efforts;
