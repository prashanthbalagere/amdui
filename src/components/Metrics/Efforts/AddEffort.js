import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import moment from "moment";
import Flatpickr from "react-flatpickr";
import { SERVER_URL } from "../../../constants";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";
const token = sessionStorage.getItem("jwt");

const AddEffort = (props) => {
  const [open, setOpen] = useState(false);
  const [types, settypes] = useState([]);
  const [Vdate, setVdate] = useState(false);
  const [Vresources, setVresources] = useState(false);
  const [effort, seteffort] = useState({
    id: Math.floor(Math.random() * 90000) + 10000,
    name: "",
    startDate: null,
    endDate: null,
    numberOfResources: 0,
  });

  useEffect(() => {
    getEffortSettings(sessionStorage.getItem("projectID"));
  }, []);
  const getEffortSettings = async (projectId) => {
    await fetch(SERVER_URL + "projects/" + projectId + "/getEffortSettings", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let datares = responseData.types;
        settypes(datares);
        const item = effort;
        item.name = datares[0].id;
        seteffort(item);
      })
      .catch((err) => console.error(err));
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleChangeDate = (date, id) => {
    var data = effort;
    data.startDate = moment(date[0]).format("DD-MM-YYYY");
    data.endDate = moment(date[1]).format("DD-MM-YYYY");
    seteffort(data);
    if (effort.endDate !== null || effort.startDate !== null) {
      setVdate(false);
    }
  };
  const handleChangeResourceChange = (e) => {
    const item = effort;
    item.numberOfResources = +e.target.value;
    seteffort(item);
    if (
      effort.numberOfResources !== "0" ||
      effort.numberOfResources !== "" ||
      effort.numberOfResources !== 0
    ) {
      setVresources(false);
    }
  };
  const HandleTypeChange = (e) => {
    const item = effort;
    item.name = e.target.value;
    seteffort(item);
  };
  const addEffort = (projectId) => {
    if (effort.endDate === null || effort.startDate === null) {
      setVdate(true);
    }
    if (
      effort.numberOfResources === "0" ||
      effort.numberOfResources === "" ||
      effort.numberOfResources === 0
    ) {
      setVresources(true);
    }
    if (
      effort.numberOfResources === "0" ||
      effort.numberOfResources === "" ||
      effort.numberOfResources === 0 ||
      effort.endDate === null ||
      effort.startDate === null
    ) {
    } else {
      fetch(SERVER_URL + "projects/" + projectId + "/updateEfforts", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify([effort]),
      })
        .then((res) => {
          if (res.status === 200) {
            toast.success("Changes saved successfully", {
              position: toast.POSITION.BOTTOM_LEFT,
            });
            props.fetchAllEfforts();
            setOpen(false);
          } else {
            toast.error("Error when saving", {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        })
        .catch((err) => {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        });
    }
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <button onClick={handleClickOpen} className="default-btn" type="button">
        <i className="fas fa-plus mr-1 text-md text-white"></i> Add Effort
      </button>
      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">Add Effort</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
            <form>
              <div className="flex flex-wrap">
                <div className="w-full lg:w-12/12 px-4 mt-5">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label" htmlFor="type">
                      Type
                    </label>

                    <select
                      className="metrics-input"
                      name="type"
                      onChange={(e) => {
                        HandleTypeChange(e);
                      }}
                    >
                      {types.map((item) => {
                        return <option value={item.id}>{item.name}</option>;
                      })}
                    </select>
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">Date</label>

                    <div className="relative  w-full">
                      <Flatpickr
                        className="metrics-input pl-9"
                        options={{
                          mode: "range",
                          static: true,
                          monthSelectorType: "static",
                          position: "right",
                          dateFormat: "d/m/Y",
                          defaultDate: [null, null],
                          prevArrow:
                            '<svg className="fill-current" width="7" height="11" viewBox="0 0 7 11"><path d="M5.4 10.8l1.4-1.4-4-4 4-4L5.4 0 0 5.4z" /></svg>',
                          nextArrow:
                            '<svg className="fill-current" width="7" height="11" viewBox="0 0 7 11"><path d="M1.4 10.8L0 9.4l4-4-4-4L1.4 0l5.4 5.4z" /></svg>',
                          onReady: (selectedDates, dateStr, instance) => {
                            instance.element.value = dateStr.replace("to", "-");
                          },
                          onChange: (selectedDates, dateStr, instance) => {
                            instance.element.value = dateStr.replace("to", "-");
                          },
                          // disable:
                          //   ddates[elm.name] === undefined
                          //     ? []
                          //     : ddates[elm.name],
                        }}
                        onChange={(date, dateStr) => {
                          if (date.length === 2) {
                            handleChangeDate(date);
                          }
                        }}
                      />
                      <div className="absolute inset-0 right-auto flex items-center pointer-events-none">
                        <svg
                          className="w-4 h-4 fill-current text-gray-500 ml-3"
                          viewBox="0 0 16 16"
                        >
                          <path d="M15 2h-2V0h-2v2H9V0H7v2H5V0H3v2H1a1 1 0 00-1 1v12a1 1 0 001 1h14a1 1 0 001-1V3a1 1 0 00-1-1zm-1 12H2V6h12v8z" />
                        </svg>
                      </div>
                    </div>
                    {Vdate ? (
                      <span className="text-red-600 text-xs m-1">
                        Please select valid date
                      </span>
                    ) : null}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full">
                    <label className="metrics-label">Number of Resources</label>

                    <input
                      type="text"
                      name="value"
                      className="metrics-input"
                      onChange={(e) => handleChangeResourceChange(e)}
                    />
                  </div>
                  {Vresources ? (
                    <span className="text-red-600 text-xs m-1">
                      Please add number of resources
                    </span>
                  ) : null}
                </div>
              </div>
              <div className="flex flex-wrap justify-center py-3">
                <button
                  className="default-btn"
                  type="button"
                  onClick={() => addEffort(sessionStorage.getItem("projectID"))}
                >
                  Save
                </button>
                <button
                  className="default-btn-cancel"
                  type="button"
                  onClick={handleClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </Dialog>
    </React.Fragment>
  );
};

export default AddEffort;
