import React, { Component } from "react";
import { useHistory } from "react-router";
import GovernanceWeeklyProgress from "./GovernanceWeeklyProgress";
import GovernanceMonthlyProgress from "./GovernanceMonthlyProgress";
import GovernanceQuarterlyProgress from "./GovernanceQuarterlyProgress";

const GovernanceProgressTabs = () => {
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };
  const [openTab, setOpenTab] = React.useState(1);
  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">Governance</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button onClick={handleClose} className="default-btn" type="button">
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap">
        <ul className="flex mb-0 list-none pb-1 pl-1 flex-row" role="tablist">
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 1
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(1);
              }}
              data-toggle="tab"
              href="#link1"
              role="tablist"
            >
              <i className="fas fa-calendar-week text-base mr-1"></i> Weekly
            </a>
          </li>
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 2
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(2);
              }}
              data-toggle="tab"
              href="#link2"
              role="tablist"
            >
              <i className="far fa-calendar text-base mr-1"></i> Monthly
            </a>
          </li>
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 3
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(3);
              }}
              data-toggle="tab"
              href="#link3"
              role="tablist"
            >
              <i className="fas fa-calendar text-base mr-1"></i> Quarterly
            </a>
          </li>
        </ul>
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 mt-2 border-t border-gray-300">
          <div className="flex-auto">
            <div className="tab-content tab-space">
              <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                <GovernanceWeeklyProgress>
                  Cheese is the best
                </GovernanceWeeklyProgress>
              </div>
              <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                <GovernanceMonthlyProgress>
                  Cheese is the best
                </GovernanceMonthlyProgress>
              </div>
              <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                <GovernanceQuarterlyProgress>
                  Cheese is the best
                </GovernanceQuarterlyProgress>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default GovernanceProgressTabs;
