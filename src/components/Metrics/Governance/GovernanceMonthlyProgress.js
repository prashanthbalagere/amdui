import React, { Component } from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import { SERVER_URL } from "../../../constants.js";
import { ToastContainer, toast } from "react-toastify";
import Select from "react-select";
import Verticaltabs from "../verticalTabs.js";
const token = sessionStorage.getItem("jwt");

class GovernanceMonthlyProgress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      progress: [],
      selectedYear: moment(new Date()).format("yyyy"),
      hide: false,
      selectedMonth: moment(new Date()).format("MMM"),
      startDate: new Date(),
    };
  }

  componentDidMount() {
    this.fetchAllModules();
  }
  fetchAllModules = () => {
    this.getAllModules(sessionStorage.getItem("projectID"));
  };

  getAllModules = (projectId) => {
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/governance/monthlyProgress/month/" +
        this.state.selectedMonth +
        "/year/" +
        this.state.selectedYear,
      {
        method: "GET",
        headers: {
          Authorization: token,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        console.log(JSON.stringify(responseData.progress));

        this.setState({
          progress: responseData.progress,
        });
      })
      .catch((err) => console.error(err));
  };
  handleChange = (date) => {
    this.setState(
      {
        startDate: date,
        selectedYear: moment(date).format("YYYY"),
        selectedMonth: moment(date).format("MMM"),
      },
      () => {
        this.fetchAllModules(this.state.selectedMonth);
      }
    );
  };

  updateProgress(projectId, updatedProgress) {
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/governance/monthlyProgress/month/" +
        this.state.selectedMonth +
        "/year/" +
        this.state.selectedYear,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify(updatedProgress),
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })

      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };
  render() {
    const { progress, startDate } = this.state;

    return (
      <div>
        <div className="flex-col md:flex-row justify-between  flex gap-4 items-start mx-1 py-1">
          <div className="w-24 py-2 ml-1">
            <p className="text-xs font-semibold text-gray-600">
              Select a Month
            </p>
            <DatePicker
              selected={startDate}
              onChange={(date) => this.handleChange(date)}
              dateFormat="MMM - yyyy"
              showMonthYearPicker
              popperPlacement="right-start"
            />
          </div>
        </div>
        <div className="flex flex-col">
          {progress.length > 0 ? (
            <Verticaltabs tabsdata={progress} triggertoSave={this.handleSave} />
          ) : null}
        </div>
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  // Save project and close modal form
  handleSave = () => {
    this.updateProgress(
      sessionStorage.getItem("projectID"),
      this.state.progress
    );
  };
}

export default GovernanceMonthlyProgress;
