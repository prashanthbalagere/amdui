import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { ToastContainer, toast } from "react-toastify";
import Weekpicker from "components/Datepickers/weekPicker.js";
import Verticaltabs from "../verticalTabs.js";
const token = sessionStorage.getItem("jwt");

class GovernanceWeeklyProgress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      progress: [],
      selectedNumber: "Week-01",
      hide: false,
      selectedOption: "Week-01",
    };
  }

  componentDidMount() {
    this.fetchAllModules("Week-01");
  }
  handleChange = (selectedOption) => {
    console.log("this.state.selectedOption :>> ", selectedOption);
    this.setState({
      selectedOption: selectedOption,
      selectedNumber: selectedOption.value,
    });
    this.fetchAllModules(selectedOption.value);
  };

  fetchAllModules = (week) => {
    this.setState({
      selectedOption: week,
      selectedNumber: week,
    });
    this.getAllModules(sessionStorage.getItem("projectID"), week);
  };

  getAllModules = (projectId, week) => {
    loaderShow();
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/governance/weeklyProgress/week/" +
        week +
        "/year/2021",
      {
        method: "GET",
        headers: {
          Authorization: token,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        loaderHide();
        console.log(
          "The modules response from the server is : " + responseData
        );

        this.setState({
          progress: responseData.progress,
        });
      })
      .catch((err) => console.error(err));
  };

  // Update estimates
  updateProgress(projectId, updatedProgress, selectItem) {
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/governance/weeklyProgress/week/" +
        selectItem +
        "/year/2021",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify(updatedProgress),
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { progress } = this.state;

    return (
      <div>
        <div className="flex-col md:flex-row justify-between  flex gap-4 items-start mx-1 py-1">
          <div className="pt-2">
            <Weekpicker weekChange={this.fetchAllModules} />
          </div>
        </div>
        <div className="flex flex-col">
          {progress.length > 0 ? (
            <Verticaltabs tabsdata={progress} triggertoSave={this.handleSave} />
          ) : null}
        </div>
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  // Save project and close modal form
  handleSave = () => {
    this.updateProgress(
      sessionStorage.getItem("projectID"),
      this.state.progress,
      this.state.selectedNumber
    );
  };
}

export default GovernanceWeeklyProgress;
