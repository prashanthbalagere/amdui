import React, { useState, useEffect } from "react";
import { BrowserRouter as Link } from "react-router-dom";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import { SERVER_URL } from "../../../constants.js";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";
const token = sessionStorage.getItem("jwt");

const JIRASettings = (props) => {
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [projectid, setProjectid] = useState();
  const [settings, setSettings] = useState(null);

  const fetchProject = () => {
    setProjectid(sessionStorage.getItem("projectID"));
    getAllJirasettings(sessionStorage.getItem("projectID"));
  };

  const getAllJirasettings = (projectId) => {
    loaderShow();
    fetch(SERVER_URL + "projects/" + projectId + "/jiraSettings", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let almvalues = responseData.JiraSettings;
        setSettings({ ...almvalues });
        loaderHide();
      })
      .catch((err) => console.error(err));
  };
  useEffect(() => {
    fetchProject();

    return () => {};
  }, []);

  const onSubmit = (data) => {
    let sendval = data;

    fetch(SERVER_URL + "projects/" + projectid + "/jiraSettings", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(sendval),
    })
      .then((res) => {
        console.log("res :>> ", res);
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  };

  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">JIRA Settings</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button className="default-btn" type="button" onClick={handleClose}>
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="w-full">
          {settings !== null ? (
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex flex-wrap bg-white p-4 px-1 rounded-lg shadow-lg">
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">JIRA Query API</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={settings.jiraQueryAPI}
                      name="jiraQueryAPI"
                      {...register("jiraQueryAPI", {
                        required: "Please enter a API",
                      })}
                    />
                    {errors.jiraQueryAPI && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.jiraQueryAPI.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">JIRA User ID</label>
                    <input
                      type="email"
                      autoComplete="off"
                      className="metrics-input"
                      defaultValue={settings.jiraUserId}
                      name="jiraUserId"
                      {...register("jiraUserId", {
                        required: "Please enter a JIRA ID",
                      })}
                    />
                    {errors.jiraUserId && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.jiraUserId.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">JIRA User Passcode</label>
                    <input
                      type="password"
                      autoComplete="off"
                      className="metrics-input"
                      defaultValue={settings.jiraUserPassCode}
                      name="jiraUserPassCode"
                      {...register("jiraUserPassCode", {
                        required: "Please enter a JIRA Pasword",
                      })}
                    />
                    {errors.jiraUserPassCode && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.jiraUserPassCode.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Zephyr Base Url</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={settings.zephyrBaseUrl}
                      name="zephyrBaseUrl"
                      {...register("zephyrBaseUrl", {
                        required: "Please enter a Zephyr Base URL",
                      })}
                    />
                    {errors.zephyrBaseUrl && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.zephyrBaseUrl.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Zapi Access Key</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={settings.zapiAccessKey}
                      name="zapiAccessKey"
                      {...register("zapiAccessKey", {
                        required: "Please enter a Zephyr Access Key",
                      })}
                    />
                    {errors.zapiAccessKey && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.zapiAccessKey.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Secret Key</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={settings.secretKey}
                      name="secretKey"
                      {...register("secretKey", {
                        required: "Please enter a Secret Key",
                      })}
                    />
                    {errors.secretKey && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.secretKey.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Account ID</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={settings.accountId}
                      name="accountId"
                      {...register("accountId", {
                        required: "Please enter a Accound ID",
                      })}
                    />
                    {errors.accountId && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.accountId.message}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="flex flex-col mt-3">
                <div className="lg:w-12/12 xl:w-12/12 px-2">
                  <div className="flex flex-wrap justify-center py-2">
                    <button className="default-btn" type="submit">
                      Save
                    </button>
                    <button
                      className="default-btn-cancel"
                      type="button"
                      onClick={handleClose}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </div>
            </form>
          ) : null}
        </div>
      </div>
      <ToastContainer autoClose={3000} />
    </>
  );
};
export default JIRASettings;
