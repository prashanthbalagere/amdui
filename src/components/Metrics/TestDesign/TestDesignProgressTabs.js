import React, { Fragment } from "react";
import { SERVER_URL } from "../../../constants.js";
import { Dialog, Transition } from "@headlessui/react";
import { useHistory } from "react-router";
import TestDesignWeeklyProgress from "./TestDesignWeeklyProgress";
import TestDesignMonthlyProgress from "./TestDesignMonthlyProgress";
import { RepeatOneSharp } from "@material-ui/icons";
const token = sessionStorage.getItem("jwt");
const TestDesignProgressTabs = () => {
  const cancelButtonRef = React.useRef(null);

  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };
  const [fileselected, setFileselected] = React.useState(true);
  const [process, setProcess] = React.useState(false);
  const [fileuploaded, setFileuploaded] = React.useState(false);
  const [openTab, setOpenTab] = React.useState(1);
  const [isOpen, setIsOpen] = React.useState(false);
  const [file, setFile] = React.useState(null);
  const [filedata, setFiledata] = React.useState({ name: "", size: "" });
  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return "0 Bytes";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  };
  const uploadSingleFile = () => {
    var files = file;
    if (files === null) {
      setFileselected(false);
    } else {
      var formData = new FormData();
      formData.append("file", files);

      var xhr = new XMLHttpRequest();
      xhr.open("POST", SERVER_URL + "uploadFile");

      xhr.onload = function () {
        console.log(xhr.responseText);
        var response = JSON.parse(xhr.responseText);
        if (xhr.status === 200) {
          setFileuploaded(true);
        } else {
          setFileuploaded(false);
        }
      };

      xhr.send(formData);

      // const data = new FormData();
      // data.append("file", files);
      // fetch(SERVER_URL + "/uploadFile", {
      //   method: "POST",
      //   headers: {
      //     "Content-Type": "multipart/form-data",
      //   },
      //   body: data,
      // })
      //   .then((response) => response.json())
      //   .then((responseData) => {})
      //   .catch((err) => console.error(err));
    }
  };
  const processFile = () => {
    const projectid = sessionStorage.getItem("projectID");
    fetch(SERVER_URL + "projects/" + projectid + "/progress", {
      method: "POST",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => {
        console.log(`response`, response);
        if (response.status === 200) {
          setProcess(true);
        }
      })
      .catch((err) => console.error(err));
  };
  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">Test Design Progress</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button
              onClick={openModal}
              className="default-btn mr-2"
              type="button"
            >
              <i className="fas fa-upload mr-1 text-md text-white"></i> Upload
            </button>
            <button onClick={handleClose} className="default-btn" type="button">
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>

      <div className="flex">
        <div className="flex-none">
          <ul className="flex mb-0 list-none pb-1 pl-1 flex-row" role="tablist">
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                  (openTab === 1
                    ? "text-white bg-blue-800"
                    : "text-blue-800 bg-gray-50")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(1);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist"
              >
                <i className="fas fa-calendar-week text-base mr-1"></i> Weekly
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                  (openTab === 2
                    ? "text-white bg-blue-800"
                    : "text-blue-800 bg-gray-50")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(2);
                }}
                data-toggle="tab"
                href="#link2"
                role="tablist"
              >
                <i className="far fa-calendar text-base mr-1"></i> Monthly
              </a>
            </li>
          </ul>
        </div>
        <div className="flex-grow"></div>
        <div className="flex-none"></div>
      </div>
      <div className="flex flex-wrap">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 mt-2 border-t border-gray-300">
          <div className=" py-2 flex-auto">
            <div className="tab-content tab-space">
              <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                <TestDesignWeeklyProgress>
                  Cheese is the best
                </TestDesignWeeklyProgress>
              </div>
              <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                <TestDesignMonthlyProgress>
                  Cheese is the best
                </TestDesignMonthlyProgress>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Transition.Root show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed z-10 inset-0 overflow-y-auto"
          initialFocus={cancelButtonRef}
          onClose={closeModal}
        >
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              {fileuploaded === false ? (
                <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-2xl rounded-2xl">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    Upload Excel
                  </Dialog.Title>
                  <div className="mt-2">
                    <div>
                      <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                        <div className="space-y-1 text-center">
                          <svg
                            className="mx-auto h-12 w-12 text-gray-400"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 48 48"
                            aria-hidden="true"
                          >
                            <path
                              d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                              strokeWidth={2}
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          <div className="flex text-sm text-gray-600 text-center">
                            <label
                              htmlFor="file-upload"
                              className="px-2 mb-1 w-full relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                            >
                              <p className="w-full">Upload a file</p>
                              <input
                                id="file-upload"
                                name="file-upload"
                                type="file"
                                className="sr-only"
                                onChange={(e) => {
                                  setFile(e.target.files[0]);
                                  const a = formatBytes(e.target.files[0].size);
                                  setFiledata({
                                    ...filedata,
                                    name: e.target.files[0].name,
                                    size: a,
                                  });
                                  setFileselected(true);
                                }}
                              />
                            </label>
                            {/* <p className="pl-1">or drag and drop</p> */}
                          </div>
                          {/* <p className="text-xs text-gray-500">
                          Excel up to 10MB
                        </p> */}
                          {fileselected === true ? null : (
                            <div className="flex items-center w-full px-2 py-2 text-red-600 bg-red-100 rounded">
                              <span>
                                <i className="fas fa-exclamation-circle text-sm text-red-600 "></i>
                              </span>
                              <p className="ml-2 text-sm">
                                Please select a file to upload
                              </p>
                            </div>
                          )}
                          <p className="text-xs text-gray-500">
                            {filedata.name}
                          </p>
                          <p className="text-xs text-gray-500">
                            {filedata.size}{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="mt-4 text-center">
                    <button
                      type="button"
                      className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 rounded-md hover:bg-blue-300"
                      onClick={uploadSingleFile}
                    >
                      Upload
                    </button>
                    <button
                      type="button"
                      className="inline-flex justify-center ml-2 px-4 py-2 text-sm font-medium text-red-900 bg-red-200 rounded-md hover:bg-red-400"
                      onClick={() => {
                        setIsOpen(false);
                      }}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              ) : (
                <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-2xl rounded-2xl">
                  {process === false ? (
                    <div>
                      <Dialog.Title
                        as="h3"
                        className="text-lg font-medium leading-6 text-gray-900"
                      >
                        Please execute the file!
                      </Dialog.Title>

                      <div className="mt-2">
                        <p className="text-sm  my-5  text-center">
                          <i className="fas fa-microchip  text-6xl text-blue-500"></i>
                        </p>
                        <p className="text-sm text-gray-500 my-5 text-center ">
                          <strong>{file.name}</strong> <br /> uploaded, please
                          process the file!
                        </p>
                      </div>

                      <div className="mt-4 text-center">
                        <button
                          type="button"
                          className="inline-flex justify-center px-4 py-2 mr-2 text-sm font-medium text-white bg-blue-900 rounded-md hover:bg-blue-700 hover:text-white"
                          onClick={() => {
                            processFile();
                          }}
                        >
                          Process
                        </button>
                        <button
                          type="button"
                          className="inline-flex justify-center px-4 py-2 mr-2 text-sm font-medium text-blue-900 bg-blue-100 rounded-md hover:bg-blue-300"
                          onClick={() => {
                            setFileuploaded(false);
                          }}
                        >
                          Back
                        </button>
                        <button
                          type="button"
                          className="inline-flex justify-center px-4 py-2 mr-2 text-sm font-medium text-blue-900 bg-blue-100 rounded-md hover:bg-blue-300"
                          onClick={() => {
                            setIsOpen(false);
                          }}
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  ) : (
                    <div>
                      <Dialog.Title
                        as="h3"
                        className="text-lg font-medium leading-6 text-gray-900"
                      >
                        File Processed!
                      </Dialog.Title>

                      <div className="mt-2">
                        <p className="text-sm  my-5  text-center">
                          <i className="fas fa-check-circle text-6xl text-green-500"></i>
                        </p>
                        <p className="text-sm text-gray-500 my-5 text-center ">
                          <strong>{file.name}</strong> <br /> File processed
                          succesfully!
                        </p>
                      </div>

                      <div className="mt-4 text-center">
                        <button
                          type="button"
                          className="inline-flex justify-center px-4 py-2 mr-2 text-sm font-medium text-blue-900 bg-blue-100 rounded-md hover:bg-blue-300"
                          onClick={() => {
                            setIsOpen(false);
                          }}
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
};
export default TestDesignProgressTabs;
