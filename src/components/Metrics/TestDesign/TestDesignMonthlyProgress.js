import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import { ToastContainer, toast } from "react-toastify";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import Verticaltabs from "../verticalTabs.js";
const token = sessionStorage.getItem("jwt");

class TestDesignMonthlyProgress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      progress: [],
      selectedYear: moment(new Date()).format("yyyy"),
      hide: false,
      selectedMonth: moment(new Date()).format("MMM"),
      startDate: new Date(),
    };
  }

  componentDidMount() {
    this.fetchAllModules();
  }

  fetchAllModules = () => {
    this.getAllModules(sessionStorage.getItem("projectID"));
  };

  getAllModules = (projectId) => {
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/testDesign/monthlyProgress/month/" +
        this.state.selectedMonth +
        "/year/" +
        this.state.selectedYear,
      {
        method: "GET",
        headers: {
          Authorization: token,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        console.log(JSON.stringify(responseData.progress));

        this.setState({
          progress: responseData.progress,
        });
      })
      .catch((err) => console.error(err));
  };

  handleChange = (date) => {
    this.setState(
      {
        startDate: date,
        selectedYear: moment(date).format("YYYY"),
        selectedMonth: moment(date).format("MMM"),
      },
      () => {
        this.fetchAllModules(this.state.selectedMonth);
      }
    );
  };

  // Update estimates
  updateProgress(projectId, updatedProgress) {
    fetch(
      SERVER_URL +
        "projects/" +
        projectId +
        "/testDesign/monthlyProgress/month/" +
        this.state.selectedMonth +
        "/year/" +
        this.state.selectedYear,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify(updatedProgress),
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })

      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }

  handleClose = () => {
    this.props.history.push("/dashboard/");
  };

  render() {
    const { progress, startDate } = this.state;

    return (
      <div className="">
        <div className="flex-col md:flex-row justify-between  flex gap-4 items-start mx-1 py-1">
          <div className="w-24 py-2 ml-1">
            <p className="text-xs font-semibold text-gray-600">
              Select a Month
            </p>

            <DatePicker
              selected={startDate}
              onChange={(date) => this.handleChange(date)}
              dateFormat="MMM - yyyy"
              showMonthYearPicker
              popperPlacement="right-start"pauar
            />
          </div>
        </div>
        <div className="flex flex-col">
          {progress.length > 0 ? (
            <Verticaltabs tabsdata={progress} triggertoSave={this.handleSave} />
          ) : (
            <p className="text-center mt-5">
              Modules missing. Please add modules before you estimate the
              project.
            </p>
          )}
        </div>
        <ToastContainer autoClose={3000} />
      </div>
    );
  }

  handleSave = () => {
    if (
      this.state.progress.find((x) => x.name === "Actual End Date Milestone")
        .value === "" ||
      this.state.progress.find((x) => x.name === "Actual Start Date Milestone")
        .value === ""
    ) {
      toast.error("Enter valid dates", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    } else {
      this.updateProgress(
        sessionStorage.getItem("projectID"),
        this.state.progress
      );
    }
  };
}

export default TestDesignMonthlyProgress;
