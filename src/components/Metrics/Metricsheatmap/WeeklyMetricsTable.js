import React, { Component, Fragment, useState, useEffect, useRef } from "react";
import { Dialog, Transition, Menu } from "@headlessui/react";
import { useTable } from "react-table-7";
import { SERVER_URL } from "../../../constants.js";
const token = sessionStorage.getItem("jwt");

const defaultPropGetter = () => ({});
const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <input
        type="checkbox"
        className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
        ref={resolvedRef}
        {...rest}
      />
    );
  }
);

const WeeklyMetricsHeatMap = ({ metricsGenarated }) => {
  const [columnNames, setcolumnNames] = useState([]);
  const [data, setdata] = useState([]);
  const [metrics, setmetrics] = useState(false);

  const columns = columnNames.map((item) => {
    return {
      Header: item.name,
      accessor: (d) => d[item.acc],
      Cell: (props) => {
        return String(props.cell.value.value);
      },
    };
  });

  useEffect(
    (props) => {
      if (!metrics) {
        if (metricsGenarated) {
          getAllModules(sessionStorage.getItem("projectID"));
        }
      }
    },
    [metricsGenarated]
  );

  const Table = ({
    columns,
    data,
    getHeaderProps = defaultPropGetter,
    getColumnProps = defaultPropGetter,
    getRowProps = defaultPropGetter,
    getCellProps = defaultPropGetter,
  }) => {
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      rows,
      allColumns,
      prepareRow,
      getToggleHideAllColumnsProps,
    } = useTable({
      columns,
      data,
      initialState: {
        hiddenColumns: [
          "Year",
          "Month",
          "Api Automation Test Execution Productivity",
          "Regression Automation Test Execution Productivity",
          "Downtime And Impact Analysis Design Avg Productivity Lost",
          "Test Case Not Executed Automation",
          "Down time And Impact Analysis Execution Avg Productivity Lost",
          "Test Case Not Executed Manual",
        ],
      },
    });

    return (
      <div className="overflow-scroll">
        <div className=" ">
          <Menu as="div" className=" ">
            <Menu.Button className="bg-blue-700 px-1.5 align-middle rounded text-white absolute right-0 top-20 hover:bg-blue-200 hover:text-blue-900">
              <i className="fas fa-filter text-xs "></i>
            </Menu.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 w-8/12 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="p-5 ">
                  <div>
                    <div className="mt-0.5 grid grid-cols-2 gap-1">
                      <div className="text-indigo-900 text-sm font-medium">
                        <IndeterminateCheckbox
                          {...getToggleHideAllColumnsProps()}
                        />{" "}
                        Toggle All
                      </div>
                      {allColumns.map((column) => {
                        return (
                          <div className="form-check">
                            <input
                              type="checkbox"
                              className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                              {...column.getToggleHiddenProps()}
                            />{" "}
                            <label
                              className="form-check-label inline-block text-indigo-900 text-sm font-medium"
                              for={column.id}
                            >
                              {column.id}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps([
                      {
                        className:
                          "bg-slate-50 text-md font-medium p-2 text-black",
                        style: column.style,
                      },
                      getColumnProps(column),
                      getHeaderProps(column),
                    ])}
                  >
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                // Merge user row props in
                <tr
                  {...row.getRowProps(getRowProps(row))}
                  className={
                    "hover:border-l hover:border-2 hover:border-gray-700 " +
                    (row.values.Month.value === "---"
                      ? "border-2 border-black"
                      : "")
                  }
                >
                  {row.cells.map((cell) => {
                    return (
                      <td
                        // Return an array of prop objects and react-table will merge them appropriately
                        {...cell.getCellProps([
                          {
                            className:
                              "text-md px-1 py-2 text-right font-medium border-b border-r border-slate-50",
                            style: cell.column.style,
                          },
                          getColumnProps(cell.column),
                          getCellProps(cell),
                        ])}
                      >
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };
  const getAllModules = (projectId, week) => {
    fetch(SERVER_URL + "projects/" + projectId + "/fetchWeeklyMetricsData", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        setcolumnNames(responseData.columnnames);
        setdata(responseData.data);
        setmetrics(true);
      })
      .catch((err) => console.error(err));
  };

  return (
    <div>
      <Table
        columns={columns}
        data={data}
        getHeaderProps={(cellInfo, rowInfo) => ({
          style: {
            width:
              cellInfo.Header === "Year"
                ? "40px"
                : "" || cellInfo.Header === "Start Date"
                ? "80px"
                : "" || cellInfo.Header === "End Date"
                ? "80px"
                : "" || cellInfo.Header === "Month"
                ? "40px"
                : "" || cellInfo.Header === "Week"
                ? "40px"
                : "6.5%",
          },
        })}
        getColumnProps={(column) => ({})}
        getCellProps={(cellInfo, rowInfo) => ({
          style: {
            backgroundColor:
              cellInfo.value.category === "R"
                ? "#dc2626"
                : null || cellInfo.value.category === "A"
                ? "#fcd34d"
                : null || cellInfo.value.category === "G"
                ? "#16a34a"
                : null,
          },
        })}
      />
    </div>
  );
};

export default WeeklyMetricsHeatMap;
