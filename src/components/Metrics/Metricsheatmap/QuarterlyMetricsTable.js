import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { SERVER_URL } from "../../../constants.js";
const token = sessionStorage.getItem("jwt");

class QuarterlyMetricsHeatMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      metricsGenarated: false,
    };
  }
  componentDidMount() {
    // this.fetchAllModules();
  }
  componentWillReceiveProps(nextProps) {
    if (!this.state.metricsGenarated) {
      if (nextProps.metricsGenarated) {
        this.getAllModules(sessionStorage.getItem("projectID"));
      }
    }
  }
  // fetchAllModules = () => {
  //   this.getAllModules(sessionStorage.getItem("projectID"));
  // };

  getAllModules = (projectId, week) => {
    fetch(SERVER_URL + "projects/" + projectId + "/fetchQuarterlyMetrics", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          metricsGenarated: true,
          data: responseData.QuarterlyMetrics === null ? [] : responseData,
        });
      })
      .catch((err) => console.error(err));
  };

  render() {
    const { data } = this.state;
    return (
      <div>
        <ReactTable
          data={data.QuarterlyMetrics}
          columns={[
            {
              Header: "Quarter",
              columns: [
                {
                  Header: "Unit",
                  id: "quarterNumber",
                  sortable: true,
                  accessor: (d) => d.quarterNumber,
                },
              ],
            },
            {
              Header: "Revenue Leakage",
              columns: [
                {
                  Header: "Unit",
                  id: "revenueLeakage",
                  sortable: true,
                  accessor: (d) => d.data[0].value,
                  getProps: (state, rowInfo) => {
                    if (rowInfo && rowInfo.row) {
                      var a = "";
                      state.data.filter(function (el) {
                        if (el.quarterNumber === rowInfo.row.quarterNumber) {
                          a = el.data[0].category;
                        }
                      });
                      return {
                        className: "code-" + a,
                      };
                    } else {
                      return {};
                    }
                  },
                },
              ],
            },
          ]}
          defaultPageSize={17}
          className=""
        />
      </div>
    );
  }
}

export default QuarterlyMetricsHeatMap;
