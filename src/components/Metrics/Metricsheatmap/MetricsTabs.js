import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { SERVER_URL } from "../../../constants";
import {
  loaderShow,
  loaderHide,
  loadermetricsShow,
  loadermetricsHide,
} from "../../../layouts/helpers";
import WeeklyMetricsHeatMap from "./WeeklyMetricsTable";
import MonthlyMetricsHeatMap from "./MonthlyMetricsTable";
import WeeklyMonthlyMetricsHeatMap from "./WeeklyMonthlyMetricsTable";
import QuarterlyMetricsHeatMap from "./QuarterlyMetricsTable";
const token = sessionStorage.getItem("jwt");
const MetricsTabs = () => {
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };
  const [openTab, setOpenTab] = React.useState(1);
  const [metricsGenarated, setmetricsGenarated] = useState(false);
  const genarateMetrics = (link) => {
    loadermetricsShow();
    fetch(SERVER_URL + "projects/" + link + "/GenerateMetrics", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          loadermetricsHide();
          setmetricsGenarated(true);
        } else {
          loadermetricsHide();
          history.push({
            pathname: "/dashboard/",
          });
        }
      })
      .catch((err) => console.error(err));
  };
  useEffect(() => {
    genarateMetrics(sessionStorage.getItem("projectID"));
  }, []);
  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">Metrics</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button onClick={handleClose} className="default-btn" type="button">
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>
      <div className="flex flex-col">
        <ul className="flex mb-0 list-none pb-1 pl-1" role="tablist">
          <li className="w-32 -mb-px mr-2 last:mr-0 text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 1
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(1);
              }}
              data-toggle="tab"
              href="#link1"
              role="tablist"
            >
              <i className="fas fa-calendar-week text-base mr-1"></i> Weekly
            </a>
          </li>
          <li className="w-32 -mb-px mr-2 last:mr-0 text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 2
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(2);
              }}
              data-toggle="tab"
              href="#link2"
              role="tablist"
            >
              <i className="far fa-calendar text-base mr-1"></i> Monthly
            </a>
          </li>
          <li className="w-44 -mb-px mr-2 last:mr-0 text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 3
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(3);
              }}
              data-toggle="tab"
              href="#link3"
              role="tablist"
            >
              <i className="far fa-calendar text-base mr-1"></i> Weekly Monthly
            </a>
          </li>
          {/* <li className="w-32 -mb-px mr-2 last:mr-0 text-center">
            <a
              className={
                "text-xs font-bold px-4 py-2 shadow-lg rounded block leading-normal " +
                (openTab === 3
                  ? "text-white bg-blue-800"
                  : "text-blue-800 bg-gray-50")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(3);
              }}
              data-toggle="tab"
              href="#link3"
              role="tablist"
            >
              <i className="fas fa-calendar text-base mr-1"></i> Quarterly
            </a>
          </li> */}
        </ul>

        <div className="p-2 mt-3 bg-white rounded-lg shadow-lg">
          <div className="">
            <div className="tab-content tab-space heatmaptable ">
              <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                <WeeklyMetricsHeatMap metricsGenarated={metricsGenarated}>
                  Cheese is the best
                </WeeklyMetricsHeatMap>
              </div>
              <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                <MonthlyMetricsHeatMap metricsGenarated={metricsGenarated}>
                  Cheese is the best
                </MonthlyMetricsHeatMap>
              </div>
              <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                <WeeklyMonthlyMetricsHeatMap
                  metricsGenarated={metricsGenarated}
                >
                  Cheese is the best
                </WeeklyMonthlyMetricsHeatMap>
              </div>
              {/* <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                <div className="w-1/4">
                  <QuarterlyMetricsHeatMap metricsGenarated={metricsGenarated}>
                    Cheese is the best
                  </QuarterlyMetricsHeatMap>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default MetricsTabs;
