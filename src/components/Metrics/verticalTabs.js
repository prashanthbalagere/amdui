import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

class Verticaltabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: props.tabsdata,
      // progress: [
      //   {
      //     id: 1,
      //     name: "Number Of Testable Requirements Covered This Week",
      //     type: "number",
      //     value: "0",
      //     category: "Project Level Test Design Progress",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 2,
      //     name: "lorem ipsum||Number Of Manual TestCases Designed For The Week",
      //     type: "number",
      //     value: "0",
      //     category: "Module Level Test Design Progress",
      //     source: "Automation",
      //     override: false,
      //   },
      //   {
      //     id: 3,
      //     name: "lorem ipsum||Number Of Regression Automation Test Scripts Designed For The Week",
      //     type: "number",
      //     value: "0",
      //     category: "Module Level Test Design Progress",
      //     source: "Automation",
      //     override: false,
      //   },
      //   {
      //     id: 4,
      //     name: "lorem ipsum||Number Of Sanity Tests Designed For The Week",
      //     type: "number",
      //     value: "0",
      //     category: "Module Level Test Design Progress",
      //     source: "Automation",
      //     override: false,
      //   },
      //   {
      //     id: 5,
      //     name: "Total Number Of InProject Automation TestCases Automated",
      //     type: "number",
      //     value: "0",
      //     category: "Project Level Test Design Progress",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 6,
      //     name: "Total Number Of API Automation TestCases Automated",
      //     type: "number",
      //     value: "0",
      //     category: "Project Level Test Design Progress",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 7,
      //     name: "Total Number Of Test Design Review Defects Found",
      //     type: "number",
      //     value: "0",
      //     category: "Attention Required",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 8,
      //     name: "Test Planning Effort",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 9,
      //     name: "Manual TestCases Design Effort In Person Days",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 10,
      //     name: "Regression Automation Design Effort In Person Days",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 11,
      //     name: "InProject Automation Design Effort In Person days",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 12,
      //     name: "Api Automation Design Effort In Person Days",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 13,
      //     name: "Manual TestCases Maintenance Effort",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 14,
      //     name: "Automation TestCases Maintenance Effort",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //     days: true,
      //     startDate: "",
      //     endDate: "",
      //   },
      //   {
      //     id: 15,
      //     name: "Number Of Resources Authoring Manual TestCases",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 16,
      //     name: "Number Of Resources Authoring Automation TestCases",
      //     type: "number",
      //     value: "0",
      //     category: "Effort",
      //     source: "Manual",
      //     override: false,
      //   },
      //   {
      //     id: 17,
      //     name: "Environment Downtime During TestDesign Phase",
      //     type: "number",
      //     value: "0",
      //     category: "Attention Required",
      //     source: "Manual",
      //     override: false,
      //   },
      // ],

      unique: [],
      newTab: 0,
      flags: ["A", "M", "A/M", "A"],
    };
  }
  componentDidMount() {
    this.genarateUnique();

    // function getDates(startDate, endDate) {
    //   const dates = [];
    //   let currentDate = startDate;
    //   const addDays = function (days) {
    //     const date = new Date(this.valueOf());
    //     date.setDate(date.getDate() + days);
    //     return date;
    //   };
    //   while (currentDate <= endDate) {
    //     dates.push(currentDate);
    //     currentDate = addDays.call(currentDate, 1);
    //   }
    //   return dates;
    // }

    // const dates = getDates(new Date(2021, 11, 18), new Date(2021, 11, 25));
    // dates.forEach(function (date) {
    //   if (moment(date).day() !== 0 && moment(date).day() !== 6) {
    //     console.log(date);
    //   }
    // });
  }
  calculateDaysLeft(start, end) {
    let a = start.split(/-/);
    let startDate = [a[1], a[0], a[2]].join("-");
    let b = end.split(/-/);
    let endDate = [b[1], b[0], b[2]].join("-");
    const dates = [];
    let currentDate = new Date(startDate);
    const addDays = function (days) {
      const date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
    };
    while (currentDate <= new Date(endDate)) {
      dates.push(currentDate);
      currentDate = addDays.call(currentDate, 1);
    }
    let totalitems = [];
    dates.forEach(function (date) {
      if (moment(date).day() !== 0 && moment(date).day() !== 6) {
        totalitems.push(date);
      }
    });
    console.log(`totalitems`, totalitems.length);
    // if (!moment.isMoment(startDate)) startDate = moment(startDate);
    // if (!moment.isMoment(endDate)) endDate = moment(endDate);
    // console.log(`total`, endDate.diff(startDate, "days"));
    // let diffval = endDate.diff(startDate, "days");

    return totalitems.length;
  }
  handleChange = (e) => {
    var data = [...this.state.progress];
    var index = data.findIndex(
      (obj) => obj.id === +e.target.getAttribute("id")
    );
    data[index].value = e.target.value;
    this.setState({ data });
  };

  handleChangeDate = (e, date, id) => {
    var newdate = "";
    if (e === null) {
      newdate = "";
    } else {
      newdate = moment(e).format("DD-MM-yyyy");
    }
    var data = [...this.state.progress];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].value = newdate;
    this.setState({ data });
  };
  handleChangeStartDate = (e, date, id) => {
    var newdate = "";
    if (e === null) {
      newdate = "";
    } else {
      newdate = moment(e).format("DD-MM-yyyy");
    }
    var data = [...this.state.progress];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].startDate = newdate;
    data[index].value = this.calculateDaysLeft(newdate, data[index].endDate);
    this.setState({ data });
  };
  handleChangeEndDate = (e, date, id) => {
    var newdate = "";
    if (e === null) {
      newdate = "";
    } else {
      newdate = moment(e).format("DD-MM-yyyy");
    }
    var data = [...this.state.progress];
    var index = data.findIndex((obj) => obj.id === id);
    data[index].endDate = newdate;
    data[index].value = this.calculateDaysLeft(data[index].startDate, newdate);
    this.setState({ data });
  };
  genarateUnique = () => {
    let unique = [...new Set(this.state.progress.map((item) => item.category))];
    let x = unique.reduce((a, v) => ({ ...a, [v]: [] }), {});

    for (var i = 0; i < unique.length; i++) {
      this.state.progress.filter((elm) => {
        if (elm.category === unique[i]) {
          x[elm.category].push(elm.source);
        }
      });
    }
    let y = [];
    unique.forEach((it) => {
      let z = [...new Set(x[it])].toString();
      if (z === "Manual") {
        y.push("M");
      }
      if (z === "Automation") {
        y.push("A");
      }
      if (z === "Manual,Automation") {
        y.push("A / M");
      }
      if (z === "Automation,Manual") {
        y.push("A / M");
      }
    });
    console.log(`y`, y);
    this.setState({
      unique: unique,
      flags: y,
    });
  };
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };
  handleSave = () => {
    this.props.triggertoSave(this.state.progress);
  };
  componentWillReceiveProps(nextProps) {
    this.setState({ progress: nextProps.tabsdata });
  }
  render() {
    const { progress, newTab, unique, flags } = this.state;
    return (
      <div>
        <div className="flex px-1 py-1">
          <div className="flex px-4 py-3 bg-white rounded-lg shadow-lg">
            <ul
              className="flex-grow w-80 relative mb-0 list-none pb-4 flex-row "
              role="tablist"
            >
              {unique.map((item, index) => (
                <li
                  className="my-1 w-100 flex-auto text-left"
                  key={`${index}-${item}`}
                >
                  <span className="absolute right-0 mt-1">
                    <span className="text-xs py-0.5 px-1 mt-2 mr-2 rounded bg-white text-black">
                      {flags[index]}
                    </span>
                  </span>

                  <a
                    className={
                      "text-xs font-bold capitalize px-4 py-2 block leading-normal " +
                      (newTab === index
                        ? "text-white bg-blue-800 "
                        : "text-gray-900 bg-blue-100 hover:bg-blue-800 hover:text-white")
                    }
                    onClick={(e) => {
                      e.preventDefault();
                      this.setState({
                        newTab: index,
                      });
                    }}
                    data-toggle="tab"
                    href={`#tab${index}`}
                    role="tablist"
                    key={index}
                  >
                    {item}
                  </a>
                </li>
              ))}
            </ul>
          </div>
          <div className="relative  min-w-0 break-words w-full px-4 py-3  ml-3 bg-white rounded-lg shadow-lg">
            <div className="">
              <div className="tab-content tab-space">
                {unique.map((item, index) => (
                  <div
                    className={newTab === index ? "block" : "hidden"}
                    id={`tab${index}`}
                    key={index}
                  >
                    <table className="p-2 items-center w-full bg-white border-collapse my-1">
                      <thead>
                        <tr>
                          <th
                            className={
                              "px-6 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Input
                          </th>
                          <th
                            className={
                              "px-6 w-36 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Source
                          </th>

                          <th
                            className={
                              "px-6 w-56 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Progress
                          </th>
                          {item === "Effort" ? (
                            <th
                              className={
                                "px-6 w-36 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                              }
                            >
                              Start Date
                            </th>
                          ) : null}
                          {item === "Effort" ? (
                            <th
                              className={
                                "px-6 w-36 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                              }
                            >
                              End Date
                            </th>
                          ) : null}
                        </tr>
                      </thead>
                      <tbody>
                        {progress.map((elm) => {
                          if (elm.category === item) {
                            return (
                              <tr key={`${elm.id}-${elm.value}`}>
                                <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap">
                                  {elm.name}
                                </td>
                                <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap text-center">
                                  <span
                                    className={
                                      "text-xs text-white px-1 py-0.5 rounded-sm " +
                                      (elm.source === "Manual"
                                        ? "bg-green-600 "
                                        : "bg-indigo-500")
                                    }
                                  >
                                    {elm.source}
                                  </span>
                                </td>
                                {elm.type === "number" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <input
                                      type="number"
                                      min="0"
                                      defaultValue={elm.value}
                                      id={elm.id}
                                      onBlur={this.handleChange}
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                    />
                                  </td>
                                ) : null}
                                {elm.type === "date" ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <DatePicker
                                      id={elm.id}
                                      selected={
                                        elm.value === ""
                                          ? null
                                          : new Date(
                                              moment(
                                                elm.value,
                                                "DD-MM-YYYY"
                                              ).format("MM-DD-YYYY")
                                            )
                                      }
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                      dateFormat="dd/MM/yyyy"
                                      onChange={(e, date) => {
                                        var id = elm.id;
                                        this.handleChangeDate(e, date, id);
                                      }}
                                      autoComplete="off"
                                    />
                                    <br></br>
                                    {elm.value === "" ? (
                                      <span className="text-rose-700 float-right">
                                        please enter a valid date
                                      </span>
                                    ) : null}
                                  </td>
                                ) : null}
                                {elm.days === true ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <DatePicker
                                      id={elm.id}
                                      popperPlacement="bottom-end"
                                      selected={
                                        elm.startDate === ""
                                          ? null
                                          : new Date(
                                              moment(
                                                elm.startDate,
                                                "DD-MM-YYYY"
                                              ).format("MM-DD-YYYY")
                                            )
                                      }
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                      dateFormat="dd/MM/yyyy"
                                      onChange={(e, date) => {
                                        var id = elm.id;
                                        this.handleChangeStartDate(e, date, id);
                                      }}
                                      autoComplete="off"
                                    />
                                  </td>
                                ) : null}
                                {elm.days === true ? (
                                  <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                    <DatePicker
                                      id={elm.id}
                                      popperPlacement="bottom-end"
                                      selected={
                                        elm.endDate === ""
                                          ? null
                                          : new Date(
                                              moment(
                                                elm.endDate,
                                                "DD-MM-YYYY"
                                              ).format("MM-DD-YYYY")
                                            )
                                      }
                                      className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                      dateFormat="dd/MM/yyyy"
                                      onChange={(e, date) => {
                                        var id = elm.id;
                                        this.handleChangeEndDate(e, date, id);
                                      }}
                                      autoComplete="off"
                                    />
                                  </td>
                                ) : null}
                              </tr>
                            );
                          }
                        })}
                      </tbody>
                    </table>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="lg:w-12/12 xl:w-12/12 px-2">
            <div className="flex flex-wrap justify-center py-4">
              <button
                className="default-btn"
                type="button"
                onClick={this.handleSave}
              >
                Save
              </button>
              <Link to="/dashboard">
                <button
                  className="default-btn-cancel"
                  type="button"
                  //onClick={this.handleClose}
                >
                  Cancel
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Verticaltabs;
