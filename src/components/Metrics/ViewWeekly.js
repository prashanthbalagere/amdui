import React, { useState, useEffect, Fragment } from "react";
import { loaderShow, loaderHide } from "layouts/helpers.js";
import { Dialog, Transition } from "@headlessui/react";
import { useHistory } from "react-router";
import { SERVER_URL } from "../../constants.js";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
import "react-tagsinput/react-tagsinput.css";
const token = sessionStorage.getItem("jwt");

const ViewWeekly = (props) => {
  const history = useHistory();
  const handleClose = () => {
    history.push({
      pathname: "/dashboard/",
    });
  };

  const [projectid, setProjectid] = useState(
    sessionStorage.getItem("projectID")
  );
  const [isOpen, setisOpen] = useState(false);
  const [resdata, setresdata] = useState();
  const [filtervalues, setfiltervalues] = useState([]);

  const [somedata, setsomedata] = useState([]);
  const projectIdChange = (e) => {
    setProjectid(e.target.value);
  };
  const getresdata = async (a = "testDesign", b = "totalWeeklyProgress") => {
    setresdata();
    setsomedata([]);
    loaderShow();
    await fetch(SERVER_URL + "projects/" + projectid + "/" + a + "/" + b + "", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        let res = responseData.progress;
        setresdata(res);
        let columns = res[0].map((key, id) => {
          console.log(`key`, key);
          return {
            Header: key.name,
            accessor: (d) => d[id].value,
            id: key.id,
            className: key.name.replace(/ /g, "").replace(/\|/g, ""),
            headerClassName: key.name.replace(/ /g, "").replace(/\|/g, ""),
          };
        });
        setsomedata(columns);
        let filtervals = res[0].map((key, id) => {
          return {
            name: key.name,
            item: key.name.replace(/ /g, "").replace(/\|/g, ""),
            checked: true,
          };
        });
        setfiltervalues(filtervals);
        console.log(`columns`, JSON.stringify(columns));
      })
      .catch((err) => console.error(err));
    loaderHide();
  };
  const closeModal = () => {
    setisOpen(false);
  };
  const openModal = () => {
    setisOpen(true);
  };
  const filterchange = (i, e) => {
    var data = [...filtervalues];
    var index = data.findIndex((obj) => obj.item === i);
    data[index].checked = e.target.checked;
    setfiltervalues(data);

    if (e.target.checked === false) {
      for (let el of document.querySelectorAll("." + i))
        el.style.display = "none";
    } else {
      for (let el of document.querySelectorAll("." + i))
        el.style.display = "block";
    }
  };
  useEffect(() => {
    getresdata();
    return () => {};
  }, []);

  return (
    <>
      <div className=" mb-0 py-4">
        <div className="flex flex-row">
          <div className="w-full xl:w-6/12 xl:mb-0">
            <h6 className="m-heading font-body">Summary</h6>
          </div>
          <div className="w-full xl:w-6/12 text-right">
            <button className="default-btn" type="button" onClick={handleClose}>
              <i className="fas fa-times mr-1 text-md text-white"></i> Close
            </button>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="w-full ">
          <div className="flex flex-wrap bg-white p-4 px-1 rounded-lg shadow-lg">
            <div className="w-full px-4">
              <div className="inline-block w-2/12 px-1 mt-1 pr-3">
                <div className="mb-3">
                  <label className="metrics-label">Project ID</label>
                  <input
                    className="metrics-input"
                    type="text"
                    defaultValue={projectid}
                    placeholder="Project ID"
                    onChange={projectIdChange}
                  />
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("testDesign", "totalWeeklyProgress")
                    }
                  >
                    Test Design Weekly
                  </button>
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("testDesign", "totalMonthlyProgress")
                    }
                  >
                    Test Design Monthly
                  </button>
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("testExecution", "totalWeeklyProgress")
                    }
                  >
                    Test Execution Weekly
                  </button>
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("testExecution", "totalMonthlyProgress")
                    }
                  >
                    Test Execution Monthly
                  </button>
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("governance", "totalWeeklyGovernanceProgress")
                    }
                  >
                    Governance Weekly
                  </button>
                </div>
              </div>
              <div className="inline-block px-1 mt-1 pr-1">
                <div className="mb-3">
                  <label className="metrics-label"></label>
                  <br />
                  <button
                    className="default-btn"
                    onClick={() =>
                      getresdata("governance", "totalMonthlyProgress")
                    }
                  >
                    Governance Monthly
                  </button>
                </div>
              </div>
            </div>
            <div className="w-full px-6 viewtable">
              {somedata.length > 0 ? (
                <div>
                  <button
                    onClick={openModal}
                    className="bg-blue-700 px-1.5 align-middle rounded text-white absolute right-8 top-20 hover:bg-blue-200 hover:text-blue-900"
                  >
                    <i className="fas fa-filter text-xs "></i>
                  </button>
                  <Transition appear show={isOpen} as={Fragment}>
                    <Dialog
                      as="div"
                      className="fixed inset-0 z-10 overflow-y-auto"
                      onClose={closeModal}
                    >
                      <div className="min-h-screen px-4 text-center">
                        <Transition.Child
                          as={Fragment}
                          enter="ease-out duration-300"
                          enterFrom="opacity-0"
                          enterTo="opacity-100"
                          leave="ease-in duration-200"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Dialog.Overlay className="fixed inset-0" />
                        </Transition.Child>

                        {/* This element is to trick the browser into centering the modal contents. */}
                        <span
                          className="inline-block h-screen align-middle"
                          aria-hidden="true"
                        >
                          &#8203;
                        </span>
                        <Transition.Child
                          as={Fragment}
                          enter="ease-out duration-300"
                          enterFrom="opacity-0 scale-95"
                          enterTo="opacity-100 scale-100"
                          leave="ease-in duration-200"
                          leaveFrom="opacity-100 scale-100"
                          leaveTo="opacity-0 scale-95"
                        >
                          <div className="inline-block w-4/5  p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                            <Dialog.Title
                              as="h3"
                              className="text-lg font-medium leading-6 text-gray-900"
                            >
                              Filter Columns
                            </Dialog.Title>
                            <div className="mt-2 grid grid-cols-2 gap-2">
                              {filtervalues.map((elm) => {
                                return (
                                  <div className="form-check">
                                    <input
                                      className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                      type="checkbox"
                                      value=""
                                      id={elm.item}
                                      onChange={(e) => {
                                        filterchange(elm.item, e);
                                      }}
                                      checked={elm.checked}
                                    />
                                    <label
                                      className="form-check-label inline-block text-gray-800 text-sm"
                                      for={elm.item}
                                    >
                                      {elm.name}
                                    </label>
                                  </div>
                                );
                              })}
                            </div>

                            <div className="mt-4 text-center">
                              <button
                                type="button"
                                className="inline-flex justify-center ml-2 px-4 py-2 text-sm font-medium text-red-900 bg-red-200 rounded-md hover:bg-red-400"
                                onClick={closeModal}
                              >
                                Close
                              </button>
                            </div>
                          </div>
                        </Transition.Child>
                      </div>
                    </Dialog>
                  </Transition>
                  <ReactTable
                    data={resdata}
                    columns={somedata}
                    pageSize={somedata.length}
                    className="-striped -highlight viewweeklyclass"
                  />
                </div>
              ) : (
                <p className="text-center my-5">No data found.</p>
              )}
            </div>
          </div>
        </div>
      </div>

      <ToastContainer autoClose={3000} />
    </>
  );
};
export default ViewWeekly;
