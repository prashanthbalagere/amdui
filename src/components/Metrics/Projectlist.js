import React, { Component } from "react";
import { SERVER_URL } from "../../constants.js";
import { loaderShow, loaderHide, loadermetricsShow } from "layouts/helpers.js";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";
import AddProject from "./AddProject";
import EditProject from "./EditProject";
import SyncStartEnd from "./SyncStartEnd";
import Syncdate from "./Syncdate";
import Projectoptions from "components/Dropdowns/projectoptions.js";
import Projectlinks from "components/Dropdowns/projectlinks.js";
import MetricsLink from "components/Dropdowns/MetricsLink.js";
const token = sessionStorage.getItem("jwt");
toast.configure();

class Projectlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "React",
      projects: [],
      modules: [],
      hide: false,
      showEditModal: false,
      showSyncModal: false,
      project: "",
      row: "1",
    };
  }

  componentDidMount() {
    this.fetchProjects();
  }

  // Fetch all projects
  fetchProjects = () => {
    loaderShow();
    fetch(SERVER_URL + "projects", { headers: { Authorization: token } })
      .then((response) => response.json())
      .then((responseData) => {
        loaderHide();
        console.log(
          "The response from the server for get projects is : " + responseData
        );
        this.setState({
          projects: responseData.projects,
        });
      })
      .catch((err) => console.error(err));
  };
  changeModalSync = () => {
    this.setState({
      showSyncModal: false,
    });
  };
  changeModal = () => {
    this.setState({
      showEditModal: false,
    });
  };

  onEditProjectClick = (project, link) => {
    this.setState({
      showEditModal: true,
      project: project,
      row: link,
    });
  };
  // SyncStartEnd
  onSyncClick = (project, link) => {
    this.setState({
      showSyncModal: true,
      project: project,
      row: link,
    });
  };
  // SyncStartEnd
  onEffortClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/effort");
  };

  // Update project
  updateProject(project, link) {
    fetch(SERVER_URL + "projects/" + link, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(project),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
        this.fetchProjects();
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }

  // Load Modules for the Project
  onModulesButtonClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/modules");
  };

  // Load Estimate Project option
  onEstimationButtonClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/estimate");
  };

  // Governance Progress
  onGovernanceProgressClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/governance");
  };

  // Test Design Progress
  onTestDesignProgressClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/testdesign");
  };

  // Test Execution Progress
  onTestExecutionProgressClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/testexecution");
  };

  // Test Execution Progress
  onMetricsGenerationClick = (link) => {
    console.log(`link`, link);
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/metrics");
  };

  // ALM SETTINGS
  onAlmsettingsClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/almsettings");
  };

  // JIRA SETTINGS
  onJirasettingsClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/jirasettings");
  };
  // Summary
  onSummaryClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/summary");
  };

  // Delete Project
  onDelClick = (link) => {
    if (window.confirm("Are you sure to delete?")) {
      fetch(SERVER_URL + "projects/" + link, {
        method: "DELETE",
        headers: {
          Authorization: token,
        },
      })
        .then((res) => {
          toast.success("Project deleted", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          this.fetchProjects();
        })
        .catch((err) => {
          toast.error("Error when deleting", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          console.error(err);
        });
    }
  };

  // Add new project
  addProject(project) {
    console.log("The project details are" + project);
    fetch(SERVER_URL + "projects", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(project),
    })
      .then((res) => this.fetchProjects())
      .catch((err) => console.error(err));
  }
  // Dashboard View
  onViewDashClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/ELK");
  };
  // Dashboard View
  onDashURLClick = (link) => {
    sessionStorage.setItem("projectID", link);
    this.props.history.push("/dashboard/ELKurls");
  };

  render() {
    const { hide, showEditModal, showSyncModal, project, row } = this.state;
    if (hide) {
      return null;
    }

    const columns = [
      {
        Header: "ID",
        accessor: "id",
        width: 50,
      },
      {
        Header: "Name",
        accessor: "name",
        className: "projectname",
      },
      {
        Header: "Type",
        accessor: "type",
      },
      {
        Header: "Tribe",
        accessor: "cluster",
        width: 100,
      },
      {
        Header: "Domain",
        accessor: "domain",
        width: 120,
      },
      {
        Header: "Delivery Manager",
        accessor: "deliveryManager",
        width: 130,
      },
      {
        Header: "Technology",
        accessor: "technology",
        width: 100,
      },
      {
        Header: "Category",
        accessor: "category",
        width: 100,
      },
      {
        Header: "Start Date",
        accessor: "startDate",
        width: 90,
      },
      {
        Header: "End Date",
        accessor: "endDate",
        width: 80,
      },
      {
        Header: "Last Sync Date",
        accessor: "lastSyncUpdateDate",
        width: 120,
        Cell: ({ value, row }) => <Syncdate rowvalue={value} project={row} />,
      },
      {
        Header: "",
        filterable: false,
        width: 27,
        accessor: "id",
        Cell: ({ value, row }) => (
          <Projectoptions
            rowvalue={value}
            project={row}
            onEditProject={this.onEditProjectClick}
            addModule={this.onModulesButtonClick}
            onEstimate={this.onEstimationButtonClick}
            onGovernance={this.onGovernanceProgressClick}
            onTestdesign={this.onTestDesignProgressClick}
            onTestexecution={this.onTestExecutionProgressClick}
            onAlmsettings={this.onAlmsettingsClick}
            onJirasettings={this.onJirasettingsClick}
            onSummary={this.onSummaryClick}
            onSyncStartEnd={this.onSyncClick}
            onEffort={this.onEffortClick}
          />
        ),
      },
      {
        Header: "",
        filterable: false,
        width: 27,
        accessor: "id",
        Cell: ({ value, row }) => (
          <Projectlinks
            rowvalue={value}
            project={row}
            onDashView={this.onViewDashClick}
            onDashURLs={this.onDashURLClick}
          />
        ),
      },
      {
        Header: "",
        filterable: false,
        width: 27,
        accessor: "id",
        Cell: ({ value, row }) => (
          <MetricsLink
            rowvalue={value}
            project={row}
            onMetricsClick={this.onMetricsGenerationClick}
          />
        ),
      },
    ];

    return (
      <>
        <div className=" mb-0 py-4">
          <div className="flex flex-row">
            <div className="w-full xl:w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">Projects</h6>
            </div>
            <div className="w-full xl:w-6/12 text-right">
              <AddProject
                addProject={this.addProject}
                fetchProjects={this.fetchProjects}
              />
            </div>
          </div>
        </div>
        <div className="flex-auto bg-white p-4 px-5 rounded-lg shadow-lg">
          <ReactTable
            data={this.state.projects}
            columns={columns}
            filterable={true}
            defaultPageSize={10}
          />
          <EditProject
            showModal={showEditModal}
            project={project}
            key={project.id}
            changeModal={this.changeModal}
            updateProject={this.updateProject}
            fetchProjects={this.fetchProjects}
          />
          <SyncStartEnd
            showModal={showSyncModal}
            project={project}
            projectid={row}
            changeModalSync={this.changeModalSync}
            updateProject={this.updateProject}
            fetchProjects={this.fetchProjects}
          />
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default Projectlist;
