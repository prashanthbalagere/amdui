import React, { Component } from "react";
import { SERVER_URL } from "../../../constants.js";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { ToastContainer, toast } from "react-toastify";

import AddModule from "./AddModule";
import EditModule from "./EditModule";
const token = sessionStorage.getItem("jwt");

class MyModulesList extends Component {
  constructor(props) {
    super(props);
    this.state = { projectid: 0, projects: [], modules: [], hide: false };
  }

  componentDidMount() {
    this.fetchAllModules();
  }

  fetchAllModules = () => {
    this.setState({
      projectid: sessionStorage.getItem("projectID"),
    });
    this.getAllModules(sessionStorage.getItem("projectID"));
  };

  getAllModules = (projectId) => {
    fetch(SERVER_URL + "projects/" + projectId + "/modules", {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.log(
          "The modules response from the server is : " + responseData
        );
        this.setState({
          modules: responseData.modules,
        });
      })
      .catch((err) => console.error(err));
  };

  // Add new module
  addModule(currentProjectId, module) {
    fetch(SERVER_URL + "projects/" + currentProjectId + "/addModule", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(module),
    })
      .then((res) => {
        if (res.status === 200) {
          this.fetchAllModules();
          toast.success("Module added successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        } else {
          toast.error("Error when saving", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      })
      .catch((err) => {
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      });
  }

  // Update module
  updateModule = (module, link, projectId) => {
    fetch(SERVER_URL + "projects/" + projectId + "/updateModule/" + link, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(module),
    })
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          this.getAllModules(this.state.projectid);
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  };

  // Delete Project
  onDelClick = (link) => {
    this.deleteModule(link, sessionStorage.getItem("projectID"));
  };

  handleClose = () => {
    this.props.history.push("/dashboard/");
    //ReactDOM.render(<Projectlist />, document.getElementById("root"));
  };

  // Delete Project
  deleteModule = (link, projectId) => {
    console.log("Inside delete 1");
    if (window.confirm("Are you sure to delete?")) {
      fetch(SERVER_URL + "projects/" + projectId + "/modules/" + link, {
        method: "DELETE",
      })
        .then((res) => {
          toast.success("module deleted", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          this.fetchAllModules();
        })
        .catch((err) => {
          toast.error("Error when deleting", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          console.error(err);
        });
    }
  };

  render() {
    const columns = [
      {
        width: 200,
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Estimated Manual Test Cases",
        accessor: "estimatedManualTestCases",
      },
      {
        Header: "Estimated Regression Automation Test Cases",
        accessor: "estimatedRegressionAutomationTestCases",
      },
      {
        Header: "Estimated Sanity Automation Test Cases",
        accessor: "estimatedSanityAutomationTestCases",
      },
      {
        width: 100,
        Header: "Complexity",
        accessor: "complexity",
      },
      {
        sortable: false,
        filterable: false,
        width: 40,
        accessor: "id",
        Cell: ({ value, row }) => (
          <EditModule
            module={row}
            link={value}
            updateModule={this.updateModule}
            fetchAllModules={this.fetchAllModules}
          />
        ),
      },
    ];

    return (
      <>
        <div className=" mb-0 py-4">
          <div className="flex flex-row">
            <div className="w-full xl:w-6/12 xl:mb-0">
              <h6 className="m-heading font-body">Modules</h6>
            </div>
            <div className="w-full xl:w-6/12 text-right">
              <AddModule
                addModule={this.addModule}
                fetchAllModules={this.fetchAllModules}
              />
              <button
                onClick={this.fetchAllModules}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-sync-alt mr-1 text-md text-white"></i>{" "}
                Refresh
              </button>
              <button
                onClick={this.handleClose}
                className="default-btn"
                type="button"
              >
                <i className="fas fa-times mr-1 text-md text-white"></i> Close
              </button>
            </div>
          </div>
        </div>
        <div className="flex-auto bg-white p-4 px-5 rounded-lg shadow-lg">
          <ReactTable
            data={this.state.modules}
            columns={columns}
            filterable={true}
            defaultPageSize={10}
          />
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default MyModulesList;
