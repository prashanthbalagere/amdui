import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import { useForm } from "react-hook-form";
import { SERVER_URL } from "../../../constants.js";
const token = sessionStorage.getItem("jwt");

const EditModule = (props) => {
  const [open, setOpen] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    props.updateModule(
      data,
      props.module.id,
      sessionStorage.getItem("projectID")
    );

    handleClose();
  };

  const handleClose = () => {
    setOpen(false);
    //props.fetchAllModules();
  };

  const [module, setModule] = useState({
    id: "",
    name: "",
    estimatedManualTestCases: "",
    estimatedAutomationTestCases: "",
    estimatedRegressionAutomationTestCases: "",
    estimatedSanityAutomationTestCases: "",
    complexity: "",
  });

  const handleClickOpen = () => {
    setModule({
      id: props.module.id,
      name: props.module.name,
      estimatedManualTestCases: props.module.estimatedManualTestCases,
      estimatedAutomationTestCases: props.module.estimatedAutomationTestCases,
      estimatedRegressionAutomationTestCases:
        props.module.estimatedRegressionAutomationTestCases,
      estimatedSanityAutomationTestCases:
        props.module.estimatedSanityAutomationTestCases,
      complexity: props.module.complexity,
    });
    setOpen(true);
  };

  return (
    <React.Fragment>
      <button
        onClick={handleClickOpen}
        className="optionsbtn bg-blue-600 px-1 py-0.5 hover:bg-blue-800 rounded"
        type="button"
      >
        <i className="fas fa-pen text-xs text-white"></i>
      </button>
      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">Edit Module</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex flex-wrap">
                <div className="w-full lg:w-12/12 px-4 mt-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">Module Name</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue={module.name}
                      name="name"
                      {...register("name", {
                        required: "Please enter a module name",
                      })}
                    />
                    {errors.name && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.name.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Manual TestCases
                    </label>
                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      defaultValue={module.estimatedManualTestCases}
                      name="estimatedManualTestCases"
                      {...register("estimatedManualTestCases", {
                        required:
                          "Please enter a valid estimated manual test cases",
                      })}
                    />
                    {errors.estimatedManualTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedManualTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Regression Automation TestCases
                    </label>

                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      name="estimatedRegressionAutomationTestCases"
                      defaultValue={
                        module.estimatedRegressionAutomationTestCases
                      }
                      {...register("estimatedRegressionAutomationTestCases", {
                        required:
                          "Please enter a valid estimated regression automation test cases",
                      })}
                    />
                    {errors.estimatedRegressionAutomationTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedRegressionAutomationTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Sanity Automation TestCases
                    </label>

                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      name="estimatedSanityAutomationTestCases"
                      defaultValue={module.estimatedSanityAutomationTestCases}
                      {...register("estimatedSanityAutomationTestCases", {
                        required:
                          "Please enter a valid sanity regression automation test cases",
                      })}
                    />
                    {errors.estimatedSanityAutomationTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedSanityAutomationTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label" htmlFor="complexity">
                      Complexity
                    </label>
                    <select
                      className="metrics-input"
                      name="complexity"
                      defaultValue={module.complexity}
                      {...register("complexity", {
                        required: "Please select complexity",
                      })}
                    >
                      <option value="Simple">Simple</option>
                      <option value="Medium">Medium</option>
                      <option value="Hard">Hard</option>
                    </select>
                    {errors.complexity && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.complexity.message}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <hr className="my-2 border-b-1 border-blueGray-300" />
              <div className="flex flex-wrap justify-center py-3">
                <button
                  className="default-btn"
                  type="submit"
                  //onClick={handleSave}
                >
                  Save
                </button>
                <button
                  className="default-btn-cancel"
                  type="button"
                  onClick={handleClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
        {/* <DialogTitle>New Module</DialogTitle>
          <DialogContent>		  
            <input type="text" placeholder="Module Name" name="name" 
              value={module.name} onChange={handleChange}/><br/> 
            <input type="text" placeholder="Estimated Manual TestCases" name="estimatedManualTestCases" 
              value={module.estimatedManualTestCases} onChange={handleChange}/><br/>
			<input type="text" placeholder="Estimated Regression Automation TestCases" name="estimatedRegressionAutomationTestCases" 
              value={module.estimatedRegressionAutomationTestCases} onChange={handleChange}/><br/>
			<input type="text" placeholder="Complexity" name="complexity" 
              value={module.complexity} onChange={handleChange}/><br/>			  
          </DialogContent>
          <DialogActions>
			<Button variant="contained" color="primary" style={{margin: 10}} onClick={handleClose}>Cancel</Button>
			<Button variant="contained" color="secondary" style={{margin: 10}} onClick={handleSave}>Save</Button>			
          </DialogActions> */}
      </Dialog>
    </React.Fragment>
    // <div>
    //   <BiEdit onClick={handleClickOpen}>Edit</BiEdit>
    //   <Dialog open={open} onClose={handleClose}>
    //     <DialogTitle>Edit module</DialogTitle>
    //     <DialogContent>
    //       <input
    //         type="text"
    //         placeholder="Module Name"
    //         name="name"
    //         value={module.name}
    //         onChange={handleChange}
    //       />
    //       <br />
    //       <input
    //         type="text"
    //         placeholder="Estimated Manual Test Cases"
    //         name="estimatedManualTestCases"
    //         value={module.estimatedManualTestCases}
    //         onChange={handleChange}
    //       />
    //       <br />
    //       <input
    //         type="text"
    //         placeholder="Estimated Regression Automation TestCases"
    //         name="estimatedRegressionAutomationTestCases"
    //         value={module.estimatedRegressionAutomationTestCases}
    //         onChange={handleChange}
    //       />
    //       <br />
    //       <input
    //         type="text"
    //         placeholder="Complexity"
    //         name="complexity"
    //         value={module.complexity}
    //         onChange={handleChange}
    //       />
    //       <br />
    //     </DialogContent>
    //     <DialogActions>
    //       <Button
    //         variant="contained"
    //         color="primary"
    //         style={{ margin: 10 }}
    //         onClick={handleClose}
    //       >
    //         Cancel
    //       </Button>
    //       <Button
    //         variant="contained"
    //         color="secondary"
    //         style={{ margin: 10 }}
    //         onClick={handleSave}
    //       >
    //         Save
    //       </Button>
    //     </DialogActions>
    //   </Dialog>
    // </div>
  );
};

export default EditModule;
