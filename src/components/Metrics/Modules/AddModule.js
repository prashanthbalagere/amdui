import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import { SERVER_URL } from "../../../constants.js";
import { useForm } from "react-hook-form";
const token = sessionStorage.getItem("jwt");

const AddModule = (props) => {
  const [open, setOpen] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const onSubmit = (data) => {
    props.addModule(sessionStorage.getItem("projectID"), data);
    props.fetchAllModules();
    reset({});

    handleClose();
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <button onClick={handleClickOpen} className="default-btn" type="button">
        <i className="fas fa-plus mr-1 text-md text-white"></i> New Module
      </button>
      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">New Module</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex flex-wrap">
                <div className="w-full lg:w-12/12 px-4 mt-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">Module Name</label>
                    <input
                      type="text"
                      className="metrics-input"
                      defaultValue=""
                      name="name"
                      {...register("name", {
                        required: "Please enter a module name",
                      })}
                    />
                    {errors.name && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.name.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Manual TestCases
                    </label>
                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      defaultValue=""
                      name="estimatedManualTestCases"
                      {...register("estimatedManualTestCases", {
                        required:
                          "Please enter a valid estimated manual test cases",
                      })}
                    />
                    {errors.estimatedManualTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedManualTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Regression Automation TestCases
                    </label>

                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      name="estimatedRegressionAutomationTestCases"
                      defaultValue=""
                      {...register("estimatedRegressionAutomationTestCases", {
                        required:
                          "Please enter a valid estimated regression automation test cases",
                      })}
                    />
                    {errors.estimatedRegressionAutomationTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedRegressionAutomationTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label">
                      Estimated Sanity Automation TestCases
                    </label>

                    <input
                      type="number"
                      min="0"
                      className="metrics-input"
                      name="estimatedSanityAutomationTestCases"
                      defaultValue=""
                      {...register("estimatedSanityAutomationTestCases", {
                        required:
                          "Please enter a valid estimated sanity automation test cases",
                      })}
                    />
                    {errors.estimatedSanityAutomationTestCases && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.estimatedSanityAutomationTestCases.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-12/12 px-4">
                  <div className="relative w-full mb-5">
                    <label className="metrics-label" htmlFor="complexity">
                      Complexity
                    </label>
                    <select
                      className="metrics-input"
                      name="complexity"
                      {...register("complexity", {
                        required: "Please select complexity",
                      })}
                    >
                      <option value="Simple">Simple</option>
                      <option value="Medium">Medium</option>
                      <option value="Hard">Hard</option>
                    </select>
                    {errors.complexity && (
                      <div className="my-1 text-sm text-red-500">
                        {errors.complexity.message}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <hr className="my-2 border-b-1 border-blueGray-300" />
              <div className="flex flex-wrap justify-center py-3">
                <button
                  className="default-btn"
                  type="submit"
                  //onClick={handleSave}
                >
                  Save
                </button>
                <button
                  className="default-btn-cancel"
                  type="button"
                  onClick={handleClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </Dialog>
    </React.Fragment>
  );
};

export default AddModule;
