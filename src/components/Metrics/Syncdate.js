import React, { useState } from "react";
import DayPicker from "react-day-picker";
import { ToastContainer, toast } from "react-toastify";
import "react-day-picker/lib/style.css";
import { Popover, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { SERVER_URL } from "../../constants";
const token = sessionStorage.getItem("jwt");
class Syncdate extends React.Component {
  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.state = {
      selectedDay: props.project.lastSyncUpdateDate,
    };
    console.log(`props.project`, props.project);
  }
  handleDayClick(day, { selected }) {
    this.setState({
      selectedDay: selected
        ? undefined
        : day.toLocaleDateString("en-GB").replace(/\//g, "-"),
    });
    fetch(
      SERVER_URL +
        "projects/" +
        this.props.project.id +
        "/setLastSyncUpdateDate?lastUpdateDate=" +
        day.toLocaleDateString("en-GB").replace(/\//g, "-"),
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      }
    )
      .then((res) => {
        if (res.status === 200) {
          toast.success("Changes saved successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          this.inputElement.click();
        }
      })
      .catch((err) =>
        toast.error("Error when saving", {
          position: toast.POSITION.BOTTOM_LEFT,
        })
      );
  }

  render() {
    const { selectedDay } = this.state;
    return (
      <>
        <div className="w-22 px-4 absolute ">
          <Popover className="relative">
            {({ open }) => (
              <>
                <Popover.Button
                  ref={(input) => (this.inputElement = input)}
                  className={`
              ${open ? "" : "text-opacity-90"}
              optionsbtn bg-blue-600 px-1 py-0.5 hover:bg-blue-800 rounded font-medium text-white`}
                >
                  <span>{selectedDay}</span>
                </Popover.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel className="absolute z-10 w-80 px-4 mt-3 transform -left-56">
                    <div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                      <div className="relative grid gap-2 bg-white p-2">
                        <DayPicker
                          selectedDays={this.state.selectedDay}
                          onDayClick={this.handleDayClick}
                        />
                      </div>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>
        <ToastContainer autoClose={3000} />
      </>
    );
  }
}

export default Syncdate;
