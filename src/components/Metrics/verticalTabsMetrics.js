import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VerticaltabsMetrics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estimates: props.tabsdata,
      unique: [],
      newTab: 0,
    };
  }
  componentDidMount() {
    this.genarateUnique();
  }
  handleChangeLCL = (e) => {
    var data = [...this.state.estimates];
    var index = data.findIndex(
      (obj) => obj.id === +e.target.getAttribute("id")
    );
    data[index].lcl = e.target.value;
    this.setState({ data });
  };
  handleChangeUCL = (e) => {
    var data = [...this.state.estimates];
    var index = data.findIndex(
      (obj) => obj.id === +e.target.getAttribute("id")
    );
    data[index].ucl = e.target.value;
    this.setState({ data });
  };
  handleChangeGOAL = (e) => {
    var data = [...this.state.estimates];
    var index = data.findIndex(
      (obj) => obj.id === +e.target.getAttribute("id")
    );
    data[index].goal = e.target.value;
    this.setState({ data });
  };

  genarateUnique = () => {
    let unique = [
      ...new Set(this.state.estimates.map((item) => item.category)),
    ];
    this.setState({
      unique: unique,
    });
  };
  handleClose = () => {
    this.props.history.push("/dashboard/");
  };
  handleSave = () => {
    this.props.triggertoSave(this.state.estimates);
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ estimates: nextProps.tabsdata });
  }
  render() {
    const { estimates, newTab, unique } = this.state;
    return (
      <div>
        <div className="flex px-1 py-1">
          <div className="flex px-4 py-3 bg-white rounded-lg shadow-lg">
            <ul
              className="flex-grow w-64 relative mb-0 list-none pb-4 flex-row "
              role="tablist"
            >
              {unique.map((item, index) => (
                <li
                  className="my-1 w-100 flex-auto text-left"
                  key={`${index}-${item}`}
                >
                  <a
                    className={
                      "text-xs font-bold capitalize px-4 py-2 block leading-normal " +
                      (newTab === index
                        ? "text-white bg-blue-800 "
                        : "text-gray-900 bg-blue-100 hover:bg-blue-800 hover:text-white")
                    }
                    onClick={(e) => {
                      e.preventDefault();
                      this.setState({
                        newTab: index,
                      });
                    }}
                    data-toggle="tab"
                    href={`#tab${index}`}
                    role="tablist"
                    key={index}
                  >
                    {item}
                  </a>
                </li>
              ))}
            </ul>
          </div>
          <div className="relative  min-w-0 break-words w-full px-4 py-3  ml-3 bg-white rounded-lg shadow-lg">
            <div className="">
              <div className="tab-content tab-space">
                {unique.map((item, index) => (
                  <div
                    className={newTab === index ? "block" : "hidden"}
                    id={`tab${index}`}
                    key={index}
                  >
                    <table className="p-2 items-center w-full bg-white border-collapse my-1">
                      <thead>
                        <tr>
                          <th
                            className={
                              "px-6 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            Metric Name
                          </th>
                          <th
                            className={
                              "px-6 w-1/5 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            LCL
                          </th>
                          <th
                            className={
                              "px-6 w-1/5 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            UCL
                          </th>
                          <th
                            className={
                              "px-6 w-1/5 align-middle text-center border border-solid py-1 text-xs uppercase whitespace-nowrap font-semibold bg-blue-800 text-white border-blue-100"
                            }
                          >
                            GOAL
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {estimates.map((elm) => {
                          if (elm.category === item) {
                            return (
                              <tr key={`${elm.metricName}-${elm.id}`}>
                                <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap">
                                  {elm.metricName}
                                </td>
                                <td className="border-t-0 px-2 align-middle text-xs whitespace-nowrap">
                                  <input
                                    type="number"
                                    min="0"
                                    defaultValue={elm.lcl}
                                    id={elm.id}
                                    onBlur={this.handleChangeLCL}
                                    className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                  />
                                </td>
                                <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                  <input
                                    type="number"
                                    min="0"
                                    defaultValue={elm.ucl}
                                    id={elm.id}
                                    onBlur={this.handleChangeUCL}
                                    className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                  />
                                </td>
                                <td className="border-t-0 px-1 align-middle text-xs whitespace-nowrap">
                                  <input
                                    type="number"
                                    min="0"
                                    defaultValue={elm.goal}
                                    id={elm.id}
                                    onBlur={this.handleChangeGOAL}
                                    className="my-0.5 text-right px-1 py-0.5 text-sm block rounded border-blue-100 w-full bg-blue-50 border-transparent focus:border-gray-200 focus:bg-white focus:ring-0"
                                  />
                                </td>
                              </tr>
                            );
                          }
                        })}
                      </tbody>
                    </table>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="lg:w-12/12 xl:w-12/12 px-2">
            <div className="flex flex-wrap justify-center py-4">
              <button
                className="default-btn"
                type="button"
                onClick={this.handleSave}
              >
                Save
              </button>
              <Link to="/dashboard">
                <button
                  className="default-btn-cancel"
                  type="button"
                  //onClick={this.handleClose}
                >
                  Cancel
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VerticaltabsMetrics;
