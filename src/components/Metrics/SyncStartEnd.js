import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import { Controller, useForm } from "react-hook-form";
import { SERVER_URL } from "../../constants";
const token = sessionStorage.getItem("jwt");

const SyncStartEnd = (props) => {
  const [open, setOpen] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    if (props.showModal) {
      setOpen(true);
    }
    getSyncDates(props.projectid);
  }, [props.projectid, props.showModal]);

  const getSyncDates = (projectId) => {
    fetch(SERVER_URL + "getAdhocSyncStartAndEndDate/" + projectId, {
      method: "GET",
      headers: {
        Authorization: token,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        const start1 = responseData.lastUpdate.startDate
          .split("-")
          .reverse()
          .join("-");
        const end2 = responseData.lastUpdate.endDate
          .split("-")
          .reverse()
          .join("-");
        setStartDate(new Date(start1));
        setEndDate(new Date(end2));
      })
      .catch((err) => console.error(err));
  };
  const updateSyncDates = (data, id) => {
    console.log(`data`, data);
    fetch(
      SERVER_URL +
        "projects/" +
        id +
        "/setAdhocSyncStartAndEndDate?endSyncDate=" +
        data.endDate +
        "&startSyncDate=" +
        data.startDate,
      {
        method: "POST",
        headers: {
          Authorization: token,
        },
      }
    )
      .then((res) => {})
      .catch((err) => {
        console.log(`err forgot password`, err);
      });
  };
  const onSubmit = (data) => {
    data.startDate = moment(startDate).format("DD-MM-yyyy");
    data.endDate = moment(endDate).format("DD-MM-yyyy");

    updateSyncDates(data, props.project.id);

    handleClose();
  };
  const handleClose = () => {
    setOpen(false);
    props.changeModalSync();
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
        <div className="relative flex flex-col min-w-0 break-words w-full shadow-lg bg-white border-0  z-0">
          <div className="rounded-t bg-blueGray-100 mb-0 px-6 py-6">
            <div className="text-center flex justify-between">
              <h6 className="m-heading">Set Sync Date</h6>
              <i
                className="fas fa-times mr-2 text-lg text-blueGray-800 cursor-pointer"
                onClick={handleClose}
              ></i>
            </div>
          </div>
          <div className="flex-auto px-4 lg:px-10 py-10 pt-4">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex flex-wrap py-5">
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">Start Date</label>
                    <Controller
                      control={control}
                      name="startDate"
                      defaultValue={startDate}
                      render={() => (
                        <DatePicker
                          className="metrics-input"
                          dateFormat="dd-MM-yyyy"
                          name="startDate"
                          onChange={(startDate) => setStartDate(startDate)}
                          selected={startDate}
                          popperPlacement="right"
                        />
                      )}
                      {...register("startDate", {
                        required: "Please select start date",
                      })}
                    />
                    {errors.startDate && (
                      <div className="text-sm text-red-500 relative -top-3">
                        {errors.startDate.message}
                      </div>
                    )}
                  </div>
                </div>
                <div className="w-full lg:w-6/12 px-1 mt-1 pr-3">
                  <div className="relative w-full mb-3">
                    <label className="metrics-label">End Date</label>
                    <Controller
                      control={control}
                      name="endDate"
                      defaultValue={endDate}
                      render={() => (
                        <DatePicker
                          className="metrics-input"
                          dateFormat="dd-MM-yyyy"
                          onChange={(endDate) => setEndDate(endDate)}
                          selected={endDate}
                          popperPlacement="left"
                        />
                      )}
                      {...register("endDate", {
                        required: "Please select end date",
                      })}
                    />
                    {errors.endDate && (
                      <div className="text-sm text-red-500 relative -top-3">
                        {errors.endDate.message}
                      </div>
                    )}
                  </div>
                </div>
              </div>

              <div className="flex flex-wrap justify-center py-3">
                <button className="default-btn" type="submit">
                  Save
                </button>
                <button
                  className="default-btn-cancel"
                  type="button"
                  onClick={handleClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default SyncStartEnd;
