
const dashboard_data = {
    "name": "Metrics Dashboard",
    "logo_url":"./assets/images/logo.png",
    "version": "1.0.0",
    "user": "admin",
    "email": "admin@admin.com",
    "copyright_text": "Copyright © 2021",
    "footer_brand": "METRICS DASHBOARD"
}

export { dashboard_data }